<!-- SET THE RIGHT LABELS AND MILESTONE (let the autocomplete guide you) -->

/label ~"release post" ~"release post item" ~"Technical Writing" ~"devops::" ~"group::"
/milestone %
/assign `@PM`

Engineer(s): `@engineers` | Product Marketing: `@PMM` | Tech Writer: `@TW` | Engineering Manager: `@EM`

Please review the guidelines for content block creation at https://about.gitlab.com/handbook/marketing/blog/release-posts/#content-blocks.
They are frequently updated, and everyone should make sure they are aware of the current standards (PM, PMM, EM, and TW). There are [separate (and slightly different) templates](https://gitlab.com/gitlab-com/www-gitlab-com/-/tree/master/data/release_posts/unreleased/samples) for primary and secondary features, bugs, deprecations, removals, and upgrade notes. _Please make sure to use the right template!_

## Links

- **Feature Issue:**
- Feature MR (optional):
- Release post (optional):

## Key dates

**By the 10th:** PMs should draft/submit for review ALL release post item content, whether they are feature or recurring blocks, earlier and no later than the 10th of the month.

**By the 16th:** All required TW reviews as well as any optional PMM and PM Director/Group Manager reviews and resulting revisions should get done no later than the 16th of the month.

**By the 17th:** Release post items need to be marked with the ~"Ready" label in order to be merged for the current release post. 

**By end of day 17th:** EMs will merge RP items with the ~"Ready" label by the end of day 11:59PM PT on the 17th. Any MRs merged into **master** after 11:59PM PDT will not make the release post and need to follow this process: 

If you need to make a change or addition to a release post item **after** 11:59PM PT on the 17th, open an MR targeting the `release-X-Y` branch and assign to the Release Post Manager, with `@mention` of the lead Tech Writer and PMM. Instructions on how to create a new or re-target an existing MR to the release X-Y branch can be found in the handbook [Contribution Instructions](https://about.gitlab.com/handbook/marketing/blog/release-posts/#instructions). **It's highly recommended the PM connect with the release post manager to make sure content can still be added prior to creating the new MR.**


Note: Drafting release post content well in advance of the 10th is highly recommended, so reviews/revisions can happen in a rolling fashion and not bottleneck against the 17th merge deadline.

## Getting ready for merge

**Reminder: Make sure any feature flags have been enabled or removed!**

Once all content is reviewed and complete, add the ~"Ready" label and assign this issue to the Engineering Manager (EM). The EM is responsible for merging as soon as the implementing feature is deployed to GitLab.com, after which this content will appear on the [GitLab.com Release page](https://about.gitlab.com/releases/gitlab-com/) and can be included in the next release post. All release post items must be merged on or before the 17th of the month. If a feature is not ready by the 17th deadline, the EM should push the release post item to the next milestone.

## PM release post item checklist

**Please only mark a section as completed once you performed all individual checks!**

- [ ] **Why?** – The benefit of this feature to the user is clearly explained
  - What is the problem we are solving for the user, and how is the situation improved?
  - Be specific about the problem, using examples so that the reader can recall the last time they had that problem.
  - Be specific about the solution, using examples so that the reader can quickly understand the improvement.
  - Describe the benefits in terms of outcomes like productivity, efficiency, velocity, communication.
  - Avoid feature language, like removing a limitation, that focusses on the product and not our users.
  - Avoid assumed knowledge, assume a customer or prospect will be linked this description without context.
- [ ] Title:
  - Length limit: 7 words (not including articles or prepositions). Exceptions may be allowed by the [Release Post Manager](https://about.gitlab.com/handbook/marketing/blog/release-posts/#release-post-manager) or the [Messaging Lead](https://about.gitlab.com/handbook/marketing/blog/release-posts/#messaging-lead).
- [ ] Content:
  - Make it clear if it is a new feature, or an improvement to an existing feature.
  - If your item is a [deprecation](https://about.gitlab.com/handbook/marketing/blog/release-posts/#deprecations), [upgrade](https://about.gitlab.com/handbook/marketing/blog/release-posts/#upgrades) or [removal](https://about.gitlab.com/handbook/marketing/blog/release-posts/#removals) reference the appropriate section in the release-posts handbook page for guidance. 
  - Make sure your [content](https://about.gitlab.com/handbook/marketing/blog/release-posts/#content) is reasonably aligned with guidance in [Writing about features](https://about.gitlab.com/handbook/product/product-processes/#communication#writing-about-features)
  - Check title is in sentence case, and feature and product names are in capital case.
  - Run the content through an automated spelling and grammar check.
  - Validate all links are functional and have [meaningful text](https://about.gitlab.com/handbook/communication/#writing-style-guidelines) for SEO (e.g., "click here" is bad link text).
- [ ] Images and Video:
  - Screenshot or video is included (required for all changes with a visible UI component). Consider preferring a speed run video since this will showcase your feature better, and also serve as a functional test to validate that it actually works as expected.
  - Check that [image size < 150KB](https://about.gitlab.com/handbook/marketing/blog/release-posts/#images).
  - Check if the image shadow is applied correctly. Add `image_noshadow: true` when an image already has a shadow.
  - Ensure the videos added to the frontmatter use /embed not /watch URLs (e.g. https://www.youtube.com/embed/eH-GuoqlweM) [more info here](https://about.gitlab.com/handbook/marketing/blog/release-posts/#videos).
  - Ensure videos and iframes added within the feature description are wrapped in `<figure class="video_container">` tags (for responsiveness).
  - Remove any remaining instructions (comments).
- [ ] Frontmatter:
  - Check feature availability frontmatter (`available_in:`) is correct: (Core, Starter, Premium, Ultimate). Make sure to set `gitlab_com: false` when the feature isn't available for GitLab.com users.
  - Check documentation link points to the latest docs (`documentation_link:`), and includes the anchor to the relevant section on the page if possible. If documentation is not yet available/merged for the feature in question, you may use a placeholder or use the link where the documentation will be added (often the engineer and tech writer know this ahead of time). Be sure to update this placeholder prior to publication if you do not use the final link.
  - Check that documentation is updated, very clearly talks about the feature (mentions it by the same name consistently in all resources).
  - Check that all links to `about.gitlab.com` content are relative URLs.
- [ ] For any top or primary features:
  - Be sure to include or revise the features.yml file as needed, as described in the [Handbook](https://about.gitlab.com/handbook/marketing/blog/release-posts/#features)
  - Be sure to label your MR ~"release post item::primary" to help the Messaging Lead find and review it for the release post. (Note that if your MR was auto-generated through your feature issue, you can skip this step)
- [ ] Assign to reviewers: Once the above are complete, assign to the Tech Writer, PMM, and Group Manager or Director to review.

## Review

When the above is complete and the content is ready for review, it should be reviewed by Product Marketing, Tech Writing,
and the product director for this area.

- [ ] (Required) Tech writer reviewed and approved
- [ ] (Recommended) PMM reviewed and approved
- [ ] (Optional) Group Manager or Director reviewed and approved (ensuring the **why** is clearly explained: what is the problem we are solving for the user, and what value are we delivering.)
- [ ] PM adds ~"Ready" label and assigns to EM for merge

### Tech writer review

Once **assigned to this merge request**, the [technical writer designated to the corresponding DevOps stage/group](https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments) will perform their review according to the criteria described below.

**Please only mark a section as completed once you performed all individual checks!**

- [ ] Feature:
  - If the feature is listed as `top` or `primary` (not `secondary`), as described in the [Handbook](https://about.gitlab.com/handbook/marketing/blog/release-posts/#features), review changes (as needed) to `features.yml`. Make sure it's associated with the appropriate stage and group.
- [ ] Image:
  - All `top` and `primary` features require an image (`png`, `jpg`, or `gif` format).
  - Make sure the image (png, jpg, or gif) is [smaller than 150 KB](https://about.gitlab.com/handbook/marketing/blog/release-posts/#images), if included.
  - If a video is included make sure it uses the `/embed/` YouTube URL path and not `?watch=` parameter
  <!--a embed URL should look like https://www.youtube.com/embed/enMumwvLAug NOT https://www.youtube.com/watch?v=enMumwvLAug -->
- [ ] Title:
  - Length limit: 7 words (not including articles or prepositions).
- [ ] Consistency:
  - Ensure that all resources (docs, release post, `features.yml` when applicable, etc.) refer to the feature with the same term / feature name.
  - Review feature [availability](https://about.gitlab.com/handbook/marketing/blog/release-posts/#feature-availability) frontmatter (`available_in:`) for consistency with the documentation: (Core, Starter, Premium, Ultimate), and `features.yml` if applicable.
- [ ] Documentation and Content:
  - Review the post and documentation for consistency. If the post says "we've added feature X", the documentation should describe something about feature X.
  - Review the content. Make sure it accurately describes the feature based on your understanding. Look for typos or grammar mistakes. Leave style and messaging for the PM and PMM.
  - Make sure there aren't acronyms readers may not understand per https://about.gitlab.com/handbook/communication/#writing-style-guidelines
    - Note: if you are unsure whether the docs were updated, check the file history looking for a recent update. If you don't find any, check with the PM. If the docs are missing (or unclear, confusing), ask the PM to request the dev who shipped the feature for an MR updating it asap and make sure to review it. If required docs changes are minor, you can choose to do it yourself to speed things up.
- [ ] Links:
  - Make sure the linked `issue_url` is correct.
  - Ensure the `documentation_link` links to the correct document and anchor, and is wrapped in single quotes.
  - Verify that all links and anchors work as intended. Do not link to the H1 (top) anchor on a docs page. Links should not redirect. Links to pages within `about.gitlab.com` are given by the relative path, not absolute.
  - `documentation_link: 'https://docs.gitlab.com/ee/#amazing'` is wrapped in single quotes and `name: "Lorem ipsum"` wrapped in double quotes.
- [ ] Code. Make sure any included code is wrapped in code blocks.
- [ ] Capitalization. Make sure to capitalize feature names. Stay consistent with the Documentation Style Guidance on [Capitalization](https://docs.gitlab.com/ee/development/documentation/styleguide.html#capitalization).
- [ ] Blank spaces. Remove unnecessary spaces (end of line spaces, double spaces, extra blank lines, and lines with only spaces).
- Notes:
  - The documentation is part of [GitLab's DoD](https://docs.gitlab.com/ee/development/contributing/merge_request_workflow.html#definition-of-done). A feature is not consider done while there's no documentation for it.
  - If there's something missing from the checklist above, you can request further action for PMs or other team members before approving this MR. You can unassign yourself while there's nothing immediate for you to do, but request to be assigned once the missing tasks are done so you can double-check and approve the MR.
  - Once all your review items have been checked, approve the merge request, check your checkbox in the [review](#review) checklist above, and unassign yourself. Your job is done!

### PMM review

**Please only mark this section as completed once you performed all individual checks!**

- [ ] PMM review
  - **problem/solution**: Does this describe the user pain points (problem) as well as how the new feature removes the paint points (solves the problem)?
  - **short/pithy:** Is this communicated clearly with the fewest words possible?
  - **tone clarify:** Is the language and sentence structure clear and grammatically correct?
  - **technical clarity**: Does the description of the feature make sense for various audiences, including folks who are not deeply familiar with GitLab?
  - [ ] Check/copyedit all your content blocks (including links/images)
  - [ ] Check/copyedit features.yml

## EM release post item checklist

- [ ] Confirm that the feature made it into the release milestone and only then
      merge this merge request. If that's not the case and the deadline has passed, update this MR's milestone accordingly and leave this unchecked.
  - Please note that any MRs getting merged prior to 11:59PM PT on the 17th will target `master`. For any MRs needing to be merged **after** 11:59PM PT on the 17th, please coordinate with the PM to create a new MR, targeting  the `release-X-Y` branch, which should then be assigned to the Release Post Manager to merge into final content assembly.
- [ ] If the feature MR is in progress, set this Release Post MR as dependent on it, to prevent merging too early.

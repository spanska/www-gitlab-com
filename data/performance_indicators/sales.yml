- name: IACV vs Plan
  base_path: "/handbook/sales/performance-indicators/"
  definition: The year-to-date cumulative sum of IACV divided by the year-to-date
    plan for IACV. Details on IACV can be found in the sales <a href="https://about.gitlab.com/handbook/sales/#incremental-annual-contract-value-iacv">handbook
    page</a>.
  target: greater than 1
  org: Sales
  is_key: true
  public: false
  health:
    level: 3
    reasons:
    - nearly met plan for April, despite COVID challenges
  urls:
  - https://app.periscopedata.com/app/gitlab/446004/Sales-KPIs?widget=6933430&udv=0
- name: Field Efficiency Ratio
  base_path: "/handbook/sales/performance-indicators/"
  definition: Field efficiency for a given month is calculated as the ratio of <a
    href="https://about.gitlab.com/handbook/sales/#net-incremental-annual-contract-value-net-iacv">Net
    IACV</a> divided by the Sales operating expenses (Net IACV / Sales Operating Expenses)
    over a rolling three month period. Sales Operating Expenses are the sum of debits
    minus the sum of credits within the "Sales" Parent Department within Netsuite
    from accounts between 6000 and 6999.
  target: Greater than 200%
  org: Sales
  is_key: true
  public: true
  health:
    level: 2
    reasons:
    - we are at 98% efficiency, but our goal is 200%
  sisense_data:
    chart: 6104902
    dashboard: 446004
    embed: v2
- name: IACV Efficiency
  base_path: "/handbook/sales/performance-indicators/"
  definition: The IACV efficiency for a given month is calculated as the ratio of
    <a href="https://about.gitlab.com/handbook/sales/#net-incremental-annual-contract-value-net-iacv">Net
    IACV</a> divided by the Sales and Marketing operating expenses (Net IACV / Sales
    and Marketing Operating Expenses) over a rolling three month period. Sales & Marketing
    Operating Expenses are the sum of debits minus the sum of credits within the "Sales"
    and "Marketing" Parent Departments within Netsuite from accounts between 6000
    and 6999. Marketing expense includes the cost of free users of gitlab.com. <a
    href="http://tomtunguz.com/magic-numbers/">Industry guidance</a> suggests that
    average performance is 0.8 with anything greater than 1.0 is considered very good.
    GitLab's target is greater than 1.
  target: greater than 1
  org: Sales
  is_key: true
  public: true
  health:
    level: 3
    reasons:
    - we are below our target but beat our adjusted plan number for FY21-Q1
  sisense_data:
    chart: 6912064
    dashboard: 446004
    embed: v2
- name: TCV vs Plan
  base_path: "/handbook/sales/performance-indicators/"
  definition: TCV for a given month divided by the planned TCV for a given month.
    Details on TCV can be found in the sales <a href="https://about.gitlab.com/handbook/sales/#total-contract-value-tcv">handbook
    page</a>.
  target: greater than 1
  org: Sales
  is_key: true
  public: false
  health:
    level: 3
    reasons:
    - we came in close to our target, despite COVID challenges
  urls:
  - https://app.periscopedata.com/app/gitlab/446004/Sales-KPIs?widget=6922179&udv=0
- name: ARR YoY
  base_path: "/handbook/sales/performance-indicators/"
  definition: ARR for a given month divided by ARR twelve months prior. Details on
    ARR can be found in the sales <a href="https://about.gitlab.com/handbook/sales/#annual-recurring-revenue-arr">handbook
    page</a>.
  target: 190%
  org: Sales
  is_key: true
  public: false
  health:
    level: 2
    reasons:
    - YoY growth is dropping
  urls:
  - https://app.periscopedata.com/app/gitlab/446004/Sales-KPIs?widget=6933786&udv=0
- name: Win Rate
  base_path: "/handbook/sales/performance-indicators/"
  definition: For a given month, the win rate is calculated as the count of closed
    won sales-assisted opportunities divided by the count of closed sales-assisted
    opportunities.
  target: 30%
  org: Sales
  is_key: true
  public: true
  health:
    level: 3
    reasons:
    - we are still winning deals, despite COVID challenges
  sisense_data:
    chart: 6924886
    dashboard: 446004
    embed: v2
- name: Percent of Ramped Reps at or Above Quota
  base_path: "/handbook/sales/performance-indicators/"
  definition: A Rep is considered ramped if they have a tenure greater than 12 months
    in that particular role at GitLab. The percentage of attainment calculation is
    the the quota divided by the gross IACV from closed won opportunities in a particular
    month. Industry average is 45%-65% of reps achieving 100% of quota.
  target: greater than 70%
  org: Sales
  is_key: true
  public: true
  health:
    level: 0
    reasons:
    - we are ironing out discrepancies in our new WIP graphs before making a health
      call
  sisense_data:
    chart: 8726847
    dashboard: 446004
    embed: v2
- name: Percent of Ramping Reps at or Above 70% of Quota
  base_path: "/handbook/sales/performance-indicators/"
  definition: A Rep is considered ramping if they have a tenure of less than or equal
    to 12 months in that particular role at GitLab. The percentage of attainment calculation
    is the the quota divided by the gross IACV from closed won opportunities in a
    particular month.
  target: greater than 80%
  org: Sales
  is_key: true
  public: true
  health:
    level: 3
    reasons:
    - we are ironing out discrepancies in our new WIP graphs before making a health
      call
  sisense_data:
    chart: 8726882
    dashboard: 446004
    embed: v2
- name: Net Retention
  base_path: "/handbook/sales/performance-indicators/"
  definition: Net Retention Calculation is extensively explained in the <a href="https://about.gitlab.com/handbook/customer-success/vision/#retention-and-reasons-for-churn">Customer
    Success's Vision page</a>.
  target: 150%
  org: Sales
  is_key: true
  public: false
  health:
    level: 3
    reasons:
    - we ended the quarter within 13% of our target
  urls:
  - https://app.periscopedata.com/app/gitlab/446004/Sales-KPIs?widget=6055135&udv=0
- name: Gross Retention
  base_path: "/handbook/sales/performance-indicators/"
  definition: Gross Retention Calculation is extensively explained in the <a href="https://about.gitlab.com/handbook/customer-success/vision/#retention-and-reasons-for-churn">Customer
    Success's Vision page</a>.
  target: 90%
  org: Sales
  is_key: true
  public: false
  health:
    level: 3
    reasons:
    - we are above target
  urls:
  - https://app.periscopedata.com/app/gitlab/446004/Sales-KPIs?widget=6062086&udv=0
- name: ProServe Revenue vs Cost
  base_path: "/handbook/sales/performance-indicators/"
  definition: For a given month, revenue from professional services divided by professional
    services operating expenses. Details on professional services revenue can be found
    in the sales <a href="https://about.gitlab.com/handbook/sales/#proserve-contract-value-pcv">handbook
    page</a>.
  target: greater than 1.1
  org: Sales
  is_key: true
  public: true
  health:
    level: 1
    reasons:
    - we do not have regular proserve revenue to meet this target
  sisense_data:
    chart: 6931495
    dashboard: 446004
    embed: v2
- name: Services Attach Rate for Strategic
  base_path: "/handbook/sales/performance-indicators/"
  definition: The percent of large parent accounts that have professional services
    associated with it or any linked SFDC accounts.
  target: greater than 80%
  org: Sales
  is_key: true
  public: true
  health:
    level: 1
    reasons:
    - only ~3% of our large parent accounts purchase professional services, but our
      goal is 80%
  sisense_data:
    chart: 6202331
    dashboard: 446004
    embed: v2
- name: Self-serve Sales Ratio
  base_path: "/handbook/sales/performance-indicators/"
  definition: For a given month, the self-serve sales ratio is calculated as the count
    of closed won web-direct opportunities divided by the count of closed won opportunities.
  target: greater than 30%
  org: Sales
  is_key: true
  public: true
  health:
    level: 1
    reasons:
    - we averaged ~10% for FY21-Q1 against a goal of 30%
  sisense_data:
    chart: 6168683
    dashboard: 446004
    embed: v2
- name: Licensed Users
  base_path: "/handbook/sales/performance-indicators/"
  definition: The number of contracted users on active paid subscriptions. Excludes
    OSS, Education, Core and other non-paid users. The data source is Zuora.
  target: 0
  org: Sales
  is_key: true
  public: true
  health:
    level: 0
    reasons:
    - we need to set a goal for this KPI
  sisense_data:
    chart: 6936917
    dashboard: 446004
    embed: v2
- name: ARPU
  base_path: "/handbook/sales/performance-indicators/"
  definition: The number of contracted users on active paid subscriptions. Excludes
    OSS, Education, Core and other non-paid users. The data source is Zuora.
  target: 0
  org: Sales
  is_key: true
  public: false
  health:
    level: 0
    reasons:
    - we need to set a goal for this KPI
  urls:
  - https://app.periscopedata.com/app/gitlab/446004/Sales-KPIs?widget=6936923&udv=0
- name: Active SMAU for Paying Customers
  base_path: "/handbook/sales/performance-indicators/"
  definition: The <a href="https://about.gitlab.com/handbook/product/metrics/#stage-monthly-active-users-smau">Stage
    Monthly Active Users (SMAU)</a> for paying customers. The data source comes from
    versions.gitlab.com and gitlab.com data, but may also depend on Salesforce depending
    on the definition of paying customers.
  target: 0
  org: Sales
  is_key: true
  public: false
  health:
    level: 0
    reasons:
    - this KPI is not currently measurable, but we are working towards gathering the
      data to measure it
- name: New Hire Location Factor
  base_path: "/handbook/sales/performance-indicators/"
  definition: The <a href="https://about.gitlab.com/handbook/people-group/people-operations-metrics/#average-location-factor">average
    location factor</a> of all newly hired team members within the last rolling 3
    month as of the end of the period.
  target: less than 0.72
  org: Sales
  is_key: true
  public: true
  health:
    level: 3
    reasons:
    - we are beating our target
  sisense_data:
    chart: 6450216
    dashboard: 446004
    embed: v2
    reasons:
    - tbd

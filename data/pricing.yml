plans:
  - id: free
    level: 0
    title: Free
    description: Develop with a team of any size
    monthly_price: 0
    features:
      - title: All stages of the DevOps lifecycle
      - title: Bring your own CI runners
      - title: Bring your own production environment
      - title: 2000 CI/CD minutes
    links:
      hosted: https://gitlab.com/users/sign_up?test=capabilities
      self_managed: /install/?test=capabilities
  - id: bronze-starter
    level: 1
    title: Bronze / Starter
    description: Control what goes<br/>into production
    monthly_price: 4
    features:
      - title: Single-project management
        has_popup: true
      - title: More control over your code
        has_popup: true
      - title: Next business day support
      - title: 2000 CI/CD minutes
    links:
      hosted: https://gitlab.com/-/subscriptions/new?plan_id=2c92a0ff5a840412015aa3cde86f2ba6&test=capabilities
      self_managed: https://customers.gitlab.com/subscriptions/new?plan_id=2c92a0ff6145d07001614efff26d15da&test=capabilities
  - id: silver-premium
    level: 2
    title: Silver / Premium
    description: Plan across<br/>multiple teams
    monthly_price: 19
    features:
      - title: Multi-project management
        has_popup: true
      - title: Code integrity controls
        has_popup: true
      - title: Multi-region support
        has_popup: true
      - title: Priority support
      - title: 10000 CI/CD minutes
    links:
      hosted: https://gitlab.com/-/subscriptions/new?plan_id=2c92a0fd5a840403015aa6d9ea2c46d6&test=capabilities
      self_managed: https://customers.gitlab.com/subscriptions/new?plan_id=2c92a0ff6145d07001614f0ca2796525&test=capabilities
  - id: gold-ultimate
    level: 3
    title: Gold / Ultimate
    description: Secure & monitor production
    monthly_price: 99
    features:
      - title: Company wide portfolio management
        has_popup: true
      - title: Advanced application security
        has_popup: true
      - title: Executive level insights
        has_popup: true
      - title: Compliance automation
        has_popup: true
      - title: Free guest users
      - title: 50000 CI/CD minutes
    links:
      hosted: https://gitlab.com/-/subscriptions/new?plan_id=2c92a0fc5a83f01d015aa6db83c45aac&test=capabilities
      self_managed: https://customers.gitlab.com/subscriptions/new?plan_id=2c92a0fe6145beb901614f137a0f1685&test=capabilities
questions:
  - question: Does GitLab offer a money-back guarantee?
    answer: >-
      Yes, we offer a 45-day money-back guarantee for any GitLab self-hosted or GitLab.com plan. See full details on
      refunds in our [terms of service](/terms/)
  - question: What are pipeline minutes?
    answer: >-
      Pipeline minutes are the execution time for your pipelines on our shared runners. Execution on your own runners
      will not increase your pipeline minutes count and is unlimited.
  - question: What happens if I reach my minutes limit?
    answer: >-
      If you reach your limits, you can
      [purchase additional CI minutes](https://customers.gitlab.com/plans), or
      upgrade your account to Silver or Gold. Your own runners can still be used even if you reach your limits.
  - question: Does the minute limit apply to all runners?
    answer: >-
      No. We will only restrict your minutes for our shared runners. If you have a
      [specific runner setup for your projects](https://docs.gitlab.com/runner/), there is no limit to your build time
      on GitLab.com.
  - question: Can I acquire a mix of licenses?
    answer: >-
      No, all users in the group need to be on the same plan.
  - question: Are GitLab Pages included in the free plan?
    answer: >-
      Absolutely, GitLab Pages will remain free for everyone.
  - question: Can I import my projects from another provider?
    answer: >-
      Yes. You can import your projects from most of the existing providers, including GitHub and Bitbucket.
      [See our documentation](https://docs.gitlab.com/ee/workflow/importing/README.html) for all your import
      options.
  - question: I already have an account, how do I upgrade?
    answer: >-
      Head over to [https://customers.gitlab.com](https://customers.gitlab.com), choose the plan that is right for you.
  - question: Do plans increase the minutes limit depending on the number of users in that group?
    answer: >-
      No. The limit will be applied to a group, no matter the number of users in that group.
  - question: How much space can I have for my repo on GitLab.com?
    answer: >-
      10GB per project.
  - question: Can I buy additional storage space for myself or my organization?
    answer: >-
      Not yet, but we are [working on it](https://gitlab.com/gitlab-org/gitlab/issues/5634), you will soon be able to
      track your storage usage across all features and buy additional storage space for
      GitLab.com.
  - question: Are there any upcoming changes to GitLab pricing?
    answer: >-
      Effective October 1, 2020, the CI/CD minutes available to the Free tier of GitLab.com will be updated to 400 minutes. Please refer to this [FAQ](https://about.gitlab.com/pricing/faq-consumption-cicd) for more information.
  - question: Do you have special pricing for open source projects or educational institutions?
    answer: >-
      Yes! We provide free Gold and Ultimate licenses to qualifying open source projects and educational institutions. Find out more by visiting our [GitLab for Open Source](/solutions/open-source/) and [GitLab for Education](/solutions/education/) program pages.
  - question: How does GitLab determine what future features fall into given tiers?
    answer: >-
      On this page we represent our [capabilities](/handbook/ceo/pricing/#capabilities) and those are meant to be filters on our [buyer-based open core](/handbook/ceo/pricing/#buyer-based-tiering-clarification) pricing model. You can learn more about how we make tiering decisions on our [pricing handbook](/handbook/ceo/pricing) page. 
  - question: Where is GitLab.com hosted?
    answer: >-
      Currently we are hosted on the Google Cloud Platform in the USA
  - question: What features do not apply to GitLab.com?
    answer: |-
      - 24/7 uptime support
      - Access to the server
      - Runs on metal
      - Highly Available setups
      - Run your own software on your instance
      - Use your configuration management software
      - Use standard Unix tools for maintenance and monitoring
      - Single package installation
      - Single configuration file
      - Basic backup and restore mechanism without additional software
      - IPv6 ready
      - AD / LDAP integration
      - Multiple LDAP / AD server support
      - Access to and ability to modify source code
      - Advanced Global Search
      - Advanced Syntax Search
      - Create and remove admins based on an LDAP group
      - Kerberos user authentication
      - Integrate with Atlassian Crowd
      - Multiple LDAP server support (compatible with AD)
      - PostgreSQL HA
      - Import from GitLab.com
      - Email all users of a project, group, or entire server
      - Limit project size at a global, group, and project level
      - Omnibus package supports log forwarding
      - Admin Control
      - Restrict SSH Keys
      - LDAP group sync
      - LDAP group sync filters
      - Live upgrade assistance
      - Audit Logs
      - Auditor users
      - Disaster Recovery
      - DevOps Score
      - Database load balancing for PostgreSQL
      - Mattermost integration
      - Object storage for artifacts
      - Object storage for LFS
      - Globally distributed cloning with GitLab Geo
      - Support for High Availability
      - Containment
      - Control
      - Retrieval
      - Configurable issue closing pattern
      - Custom Git Hooks
      - Various authentication mechanisms
      - Fast SSH Authorization
      - Instant SSL with Let's Encrypt for Omnibus GitLab
      - Plugins
      - Supports geolocation-aware DNS
      - Instance file templates
      - Instance-level Kubernetes cluster configuration
      - Smart card support
      - Instance-level kubernetes clusters
      - Show most affected projects in Group Security Dashboard
      - New configuration screen for Secure
      - Credentials Management
      - Compliance Dashboard
  - question: What is a user?
    answer: >-
      User means each individual end-user (person or machine) of Customer and/or its Affiliates (including, without
      limitation, employees, agents, and consultants thereof) with access to the Licensed Materials hereunder.
  - question: Can I add more users to my subscription?
    answer: >-
      Yes. You have a few options. You can add users to your subscription any time during the subscription period. You
      can log in to your account via the [GitLab Customer Portal](https://customers.gitlab.com) and add more seats or by
      either contacting [renewals@gitlab.com](mailto:renewals@gitlab.com) for a quote. In either case, the cost will be
      prorated from the date of quote/purchase through the end of the subscription period. You may also pay for the
      additional licences per our true-up model.
  - question: The True-Up model seems complicated, can you illustrate?
    answer: >-
      If you have 100 active users today, you should purchase a 100 user subscription. Suppose that when you renew next
      year you have 300 active users (200 extra users). When you renew you pay for a 300 user subscription and you also
      pay the full annual fee for the 200 users that you added during the year.
  - question: How does the license key work?
    answer: >-
      The license key is a static file which, upon uploading, allows GitLab Enterprise Edition to run. During license
      upload we check that the active users on your GitLab Enterprise Edition instance doesn't exceed the new number of
      users. During the licensed period you may add as many users as you want. The license key will expire after one
      year for GitLab subscribers.
  - question: What happens when my subscription is about to expire or has expired?
    answer: >-
      You will receive a new license that you will need to upload to your GitLab instance. This can be done by following
      [these instructions](https://docs.gitlab.com/ee/user/admin_area/license.html).
  - question: What happens if I decide to not renew my subscription?
    answer: >-
      14 days after the end of your subscription, your key will no longer work and GitLab Enterprise Edition will not be
      functional anymore. You will be able to downgrade to GitLab Community Edition, which is free to use.

require 'open-uri'
require 'nokogiri'
require 'cgi'
require 'openssl'

module CustomHelpers
  def icon(icon, cssclass = "", attrs = {})
    width = attrs[:width] || 76
    height = attrs[:height] || 76
    viewbox_width = attrs[:viewbox_width] || width
    viewbox_height = attrs[:viewbox_height] || height
    label = attrs[:label] || ""
    content_tag :svg, viewbox: "0 0 #{viewbox_width} #{viewbox_height}", width: width, height: height, class: cssclass, aria: { label: label }, role: "img" do
      partial "includes/icons/#{icon}.svg"
    end
  end

  def svg_image(icon, cssclass = "", attrs = {})
    data = attrs[:data] || ""
    content_tag :svg, class: cssclass, role: "img", data: data do
      partial icon
    end
  end

  def xml_feed_content(article)
    content = article.body

    content << if article.data.image_title
                 "<img src='#{data.site.url}#{article.data.image_title}' class='webfeedsFeaturedVisual' style='display: none;' />"
               else
                 "<img src='#{data.site.url}#{image_path('default-blog-image.png')}' class='webfeedsFeaturedVisual' style='display: none;' />"
               end

    h(content)
  end

  def markdown(text)
    # Scope parameter is necessary to make Markdown in YAML work properly
    # See: https://github.com/middleman/middleman/issues/653#issuecomment-9954111
    Tilt['markdown'].new { text }.render(scope: self)
  end

  def kramdown(text)
    Kramdown::Document.new(text).to_html
  end

  def team_size
    data.team.count { |entry| entry['type'] == 'person' }
  end

  def open_roles
    data.roles.select(&:open).sort_by(&:title)
  end

  def salary_avail
    data.job_families.sort_by(&:title)
  end

  def current_role_for_salary_calculator
    current_role = salary_avail.detect do |role|
      descriptions = role.description.is_a?(String) ? role.description.split : role.description

      descriptions.detect do |description|
        description.start_with?("/#{File.dirname(current_page.request_path)}")
      end
    end

    if current_role&.levels&.is_a? String
      current_role.levels = data.role_levels.send(current_role.levels)
    end

    current_role
  end

  def find_role_by_description(description)
    data.job_families.find do |role|
      role_descriptions = role.description.is_a?(String) ? role.description.split : role.description

      role_descriptions.find { |role_description| role_description == description }
    end
  end

  def kpi_list_by_org(org)
    kpis = fetch_performance_indicators_for_org(org).select { |pi| pi.is_key == true }

    partial('includes/performance_indicator_list.erb', locals: { kpis: kpis })
  end

  def color_code_grades(grade)
    color = "green"  if grade.include? 'A'
    color = "green"  if grade.include? 'B'
    color = "orange" if grade.include? 'C'
    color = "red"    if grade.include? 'D'
    color = "red"    if grade.include? 'F'

    "<span style='color:#{color};'>#{grade}</span>"
  end

  def color_code_health(level)
    case level
    when 3
      color = "green"
      text = "Okay"
    when 2
      color = "orange"
      text = "Attention"
    when 1
      color = "red"
      text = "Problem"
    else
      color = "gray"
      text = "Unknown"
    end

    "<span style='border-radius:0.2em; font-weight:bold; padding-left:1em; padding-right:1em; color:white; background-color:#{color};'>#{text}</span>"
  end

  def color_code_instrumentation(level)
    case level
    when 3
      color = "green"
      text = "Okay"
    when 2
      color = "orange"
      text = "Attention"
    when 1
      color = "red"
      text = "Problem"
    else
      color = "gray"
      text = "Unknown"
    end

    "<span style='border-radius:0.2em; font-weight:bold; padding-left:1em; padding-right:1em; color:white; background-color:#{color};'>#{text}</span>"
  end

  def key_performance_indicators(orgs)
    kpis = orgs.map { |org| fetch_performance_indicators_for_org(org) }.flatten

    kpis
      .select(&:is_key?)
      .sort_by { |pi| pi.health.level }
      .sort_by { |pi| orgs.index pi.org }
  end

  def performance_indicators_by_maturity_level
    data
      .performance_indicators
      .values
      .flatten
      .sort_by { |pi| pi_maturity_level(pi) }
  end

  def pi_maturity_level(performance_indicator)
    level = 0

    level += 1 if performance_indicator.definition
    level += 1 if performance_indicator.target
    if performance_indicator.public == false && performance_indicator.urls
      level += 1
    elsif performance_indicator.sisense_data
      level += 1
    end

    level
  end

  def pi_maturity_reasons(performance_indicator)
    reasons = []

    reasons.push("Needs a definition") unless performance_indicator.definition
    reasons.push("Needs a target") unless performance_indicator.target
    if performance_indicator.public == false && !performance_indicator.urls
      reasons.push("Needs a url")
    elsif performance_indicator.public == true && !performance_indicator.sisense_data
      reasons.push("Needs Sisense Embed Info")
    end

    reasons
  end

  def color_code_maturity(level)
    color = case level
            when 3
              "green"
            when 2
              "orange"
            when 1
              "red"
            else
              "gray"
            end

    "<span style='border-radius:0.2em; font-weight:bold; padding-left:1em; padding-right:1em; color:white; background-color:#{color};'>Level #{level} of 3</span>"
  end

  def performance_indicators_stage_and_group
    stage_pis = []
    group_pis = []

    ["Dev Section", "Enablement Section", "Ops Section", "Secure and Defend Section"].each do |section|
      fetch_performance_indicators_for_org(section).each do |pi|
        next unless pi.pi_type.present?

        if pi.pi_type.include? 'SMAU'
          stage_pis.push(pi)
        elsif pi.pi_type.include? 'Stage PPI'
          stage_pis.push(pi)
        end
        if pi.pi_type.include? 'GMAU'
          group_pis.push(pi)
        elsif pi.pi_type.include? 'GMAC'
          group_pis.push(pi)
        elsif pi.pi_type.include? 'Group PPI'
          group_pis.push(pi)
        end
      end
    end

    partial('includes/performance_indicators_stage_and_group.html.erb', locals: { stage_performance_indicators: stage_pis, group_performance_indicators: group_pis })
  end

  def performance_indicators(org)
    kpis = []
    rpis = []

    fetch_performance_indicators_for_org(org).each do |pi|
      if pi.is_key == true
        kpis.push(pi)
      else
        rpis.push(pi)
      end
    end

    partial('includes/performance_indicators.html.erb', locals: { key_performance_indicators: kpis, regular_performance_indicators: rpis })
  end

  def fetch_performance_indicators_for_org(org)
    key = org.tr(' ', '_').downcase

    data.performance_indicators.fetch(key, [])
  end

  def signed_periscope_url(data)
    # Forks may not have the API key, so just return a placeholder URL
    return 'https://about.gitlab.com/images/press/logo/preview/gitlab-logo-white-preview.png' unless ENV['PERISCOPE_EMBED_API_KEY']

    path = '/api/embedded_dashboard?data=' + CGI.escape(data.to_json)
    signature = OpenSSL::HMAC.hexdigest(OpenSSL::Digest.new('sha256'), ENV['PERISCOPE_EMBED_API_KEY'], path)

    'https://www.periscopedata.com' + path + '&signature=' + signature
  end

  def font_url(current_page)
    fonts = ["Source+Sans+Pro:200,300,400,500,600,700"]

    if current_page.data.extra_font
      fonts = fonts.concat current_page.data.extra_font
    end
    fonts = fonts.join("|")

    "//fonts.googleapis.com/css?family=#{fonts}"
  end

  def highlight_active_nav_link(link_text, url, options = {})
    options[:class] ||= ""
    options[:class] << " active" if url == current_page.url
    link_to(link_text, url, options)
  end

  def full_url(current_page)
    "#{data.site.url}#{current_page.url}"
  end

  def current_version
    ReleaseList.new.release_posts.first.version
  end

  def copy_btn_options(copy_text, tooltip_text = nil, button_class = nil)
    tooltip_text = 'Copy to clipboard' if tooltip_text.nil?
    button_class = 'copy-btn js-copy-btn' if button_class.nil?

    {
      class: "btn #{button_class}", type: 'button',
      title: tooltip_text, 'aria-label' => tooltip_text,
      data: {
        'clipboard-text' => copy_text,
        toggle: 'tooltip', placement: 'top'
      }
    }
  end

  def ci_environment?
    !!ENV['CI_SERVER']
  end

  def production_environment?
    ENV['CI_COMMIT_REF_NAME'] == 'master'
  end

  def add_extra_css(*files)
    current_page.data.extra_css ||= []
    current_page.data.extra_css |= files
  end

  def add_extra_js(*files)
    current_page.data.extra_js ||= []
    current_page.data.extra_js |= files
  end

  def heroes_size
    data.heroes.count { |entry| entry['type'] == 'person' }
  end

  def handbook_product_page?(relative_path)
    relative_path.to_s.include? 'handbook/product/'
  end

  def static_site_editor_url(site, monorepo_site_path_fragment, relative_path)
    source_file_path = ERB::Util.url_encode("master/#{monorepo_site_path_fragment}source/#{relative_path}")

    "#{site.repo}-/sse/#{source_file_path}"
  end

  def web_ide_url(site, monorepo_site_path_fragment, relative_path)
    "#{site.instance}-/ide/project/#{site.path}edit/master/-/#{monorepo_site_path_fragment}source/#{relative_path}"
  end

  def blob_editor_url(site, monorepo_site_path_fragment, relative_path)
    "#{site.repo}blob/master/#{monorepo_site_path_fragment}source/#{relative_path}"
  end
end

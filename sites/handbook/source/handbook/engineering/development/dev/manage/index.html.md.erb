---
layout: handbook-page-toc
title: Manage Stage
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

### Manage
{: #welcome}

The responsibilities of this stage are described by the [Manage product
category](/handbook/product/product-categories/#manage-stage). Manage is made up of multiple groups, each with their own categories and areas of
responsibility.

* I have a question. Who do I ask?

In GitLab issues, questions should start by @ mentioning the relevant Product Manager for the [category](/handbook/product/product-categories/#dev). GitLab employees can also use [#s_manage](https://gitlab.slack.com/messages/CBFCUM0RX).

### How we work

* In accordance with our [GitLab values](/handbook/values/).
* Transparently: nearly everything is public, we record/livestream meetings whenever possible.
* We get a chance to work on the things we want to work on.
* Everyone can contribute; no silos.
  * The goal is to have product give engineering and design the opportunity to be involved with direction and issue definition from the very beginning.
* We do an optional, asynchronous daily stand-up in our respective group stand-up channels:
  * Manage:Access [#g_manage_access_daily](https://gitlab.slack.com/archives/C01311Z0LDD)
  * Manage:Analytics [#g_manage_analytics_daily](https://gitlab.slack.com/archives/C012SB84TFU)
  * Manage:Compliance [#g_manage_compliance_daily](https://gitlab.slack.com/archives/C013E163FD0)
  * Manage:Import [#g_manage_import_daily](https://gitlab.slack.com/archives/C01099NRZ7B)

#### Direction

The direction and strategy for Manage is documented on https://about.gitlab.com/direction/manage/. This page (and the category direction
pages under the "Categories" header) is the single source of truth on where we're going and why.
* Direction pages should be reviewed regularly by Product. When updating these pages, please CC the relevant group to keep your teammates informed.
* Product should make sure that their groups understand the direction and have an opportunity to contribute to it. Consider a monthly direction AMA for your group to field questions.



#### Prioritization

Our priorities should follow [overall guidance for Product](/handbook/product/product-processes/#how-we-prioritize-work). This should be reflected in the priority label for scheduled issues:

| Priority | Description | Probability of shipping in milestone |
| ------ | ------ | ------ |
| priority::1 | **Urgent**: top priority for achieving in the given milestone. These issues are the most important goals for a release and should be worked on first; some may be time-critical or unblock dependencies. | ~100% |
| priority::2 | **High**: important issues that have significant positive impact to the business or technical debt. Important, but not time-critical or blocking others.  | ~75% |
| priority::3 | **Normal**: incremental improvements to existing features. These are important iterations, but deemed non-critical. | ~50% |
| priority::4 | **Low**: stretch issues that are acceptable to postpone into a future release. | ~25% |

#### Organizing the work

We generally follow the [Product Development Flow](/handbook/product-development-flow/#workflow-summary):
1. `workflow::problem validation` - needs clarity on the problem to solve
1. `workflow::design` - needs a clear proposal (and mockups for any visual aspects)
1. `workflow::solution validation` - needs refinement and acceptance from engineering
1. `workflow::planning breakdown` - needs a Weight estimate
1. `workflow::scheduling` - needs a milestone assignment
1. `workflow::ready for development`
1. `workflow::in dev`
1. `workflow::in review`
1. `workflow::verification` - code is in production and pending verification by the DRI engineer

Generally speaking, issues are in one of two states:
* Discovery/refinement: we're still answering questions that prevent us from starting development,
* Implementation: an issue is waiting for an engineer to work on it, or is actively being built.

Basecamp thinks about these stages in relation to the [climb and descent of a hill](https://www.feltpresence.com/hills.html).

While individual groups are free to use as many stages in the [Product Development Flow](/handbook/product-development-flow/#workflow-summary) workflow as they find useful, we should be somewhat prescriptive on how issues transition from discovery/refinement to implementation.

##### Backlog management

Backlog management is very challenging, but we try to do so with the use of labels and milestones.

###### Refinement

**The end goal is defined,** where all direct stakeholders says “yes, this is ready for development”. Some issues get there quickly, some require a few passes back and forth to figure out.

The goal is for engineers to have buy-in and feel connected to the roadmap. By having engineering included earlier on, the process can be much more natural and smooth.

To do so, engineering managers, engineers, and designers can be pinged directly from the issue. We're currently exploring [converting the Manage project into a group](https://gitlab.com/gitlab-org/manage/-/issues/16983) to be able to create groups to more easily ping group members.

To find issues that require refinement, please see the [Next Up](#next-up) label and its purpose.

###### Next Up

* To identify issues that need refinement, use the "Next Up" label.
  * The purpose of the "Next Up" label is to identify issues that are currently in _any_ workflow stage before `workflow::ready for development`. By using this "Next Up" label in addition to workflow labels, we're able to see exactly what is being refined, e.g., problem, design, solution. This helps identify which issues are closer to being ready to schedule.
* Issues shouldn't receive a milestone for a specific release (e.g. 13.0) until they've received a 👍 from both Product and Engineering. This also means the issue should not be labeled as `workflow::ready for development`.
  * Product approval is represented by an issue moving into `workflow::planning breakdown`.
  * Engineering approval is represented by an issue weight measuring its complexity.

###### Directional Milestones

Directional milestones indicates a level of intent on whether or not an issue will be scheduled in short/mid/long-term segments. They are also indicative to the level of refinement and "shovel-readiness" of a given feature or topic, i.e., something planned 3+ releases for now is likely very unrefined and not likely to be scheduled or implemented any time soon.

It's important to remember that these are **not hard commitments**, and that this should be communicated with any stakeholders that may be sensitive to seeing an issue get scheduled into a milestone other than "Backlog".

For issues that are actually being committed to the short-term roadmap, please refer to [Next Up](#next-up)

The utility in direction milestones is helping to segment the backlog and form a sort of roadmap. See the [Import Roadmap](https://gitlab.com/groups/gitlab-org/-/boards/1459561?label_name[]=group%3A%3Aimport) board for an example.

Examples of directional milestones include:

 - Next 1-3 releases
 - Next 4-7 releases
 - Next 7-13 releases

##### Breaking down or promoting issues

Depending on the complexity of an issue, it may be necessary to break down or promote issues. A couple sample scenarios may be:

- We need to do discovery on the design, before we do anything else. A "Discovery:" issue may work best here as it helps to contain the design thinking and discussion there, with the end result being transferred over to a "Implementation:" issue. These prefixes also help to organize what type of issue they are, in the case they are linked to parent issues or epics.
- The scope of work is larger than anticipated, and needs to be broken down further, e.g., it currently has a weight higher than 5. It may suit you to then promote said issue to an epic, to break it down into smaller issues to list out the different iterations or phases of work that need to happen to deliver the overall feature that was originally proposed.
- The scope of work is clear, but a bit unwieldy for one issue. It may make sense to keep the given issue as is, to keep the conversation and activity visible to everyone, but create separate child design, backend, or frontend issues to track the more nuanced progress of a given issue.

If none of the above applies, then the issue is probably fine as-is! It's likely then that the weight of this issue is quite low, e.g., 1-2.

##### Managing discussions, information, decisions, and action items in an issue.

As part of [breaking down or promoting issues](#breaking-down-or-promoting-issues), you may find that there are a significant number of threads and comments in a given issue.

It's very important that we make sure any proposal details, pending action items, and decisions are easily visible to any stakeholder coming into an issue. Therefore, it's paramount that the issue description is kept up-to-date, or otherwise broken down or promoted as per the above section.

#### Estimation

Before work can begin on an issue, we should estimate it first after a preliminary investigation.

If the scope of work of a given issue touches several disciplines (docs, design, frontend, backend, etc.) and involves significant complexity across them, consider creating separate issues for each discipline (see [an example](https://gitlab.com/gitlab-org/gitlab-ee/issues/9288)).

Issues without a weight should be assigned the "workflow::planning breakdown" label.

When estimating development work, please assign an issue an appropriate weight:

| Weight | Description (Engineering) |
| ------ | ------ |
| 1 | The simplest possible change. We are confident there will be no side effects. |
| 2 | A simple change (minimal code changes), where we understand all of the requirements. |
| 3 | A simple change, but the code footprint is bigger (e.g. lots of different files, or tests affected). The requirements are clear. |
| 5 | A more complex change that will impact multiple areas of the codebase, there may also be some refactoring involved. Requirements are understood but you feel there are likely to be some gaps along the way. |
| 8 | A complex change, that will involve much of the codebase or will require lots of input from others to determine the requirements. |
| 13 | A significant change that may have dependencies (other teams or third-parties) and we likely still don't understand all of the requirements. It's unlikely we would commit to this in a milestone, and the preference would be to further clarify requirements and/or break in to smaller Issues. |

As part of estimation, if you feel the issue is in an appropriate state for an engineer to start working on it, please add the ~"workflow::ready for development" label. Alternatively, if there are still requirements to be defined or questions to be answered that you feel an engineer won't be able to easily resolve, please add the ~"workflow::blocked" label. Issues with the `workflow::blocked` label will appear in their own column on our planning board, making it clear that they need further attention. When applying the `workflow::blocked` label, please make sure to leave a comment and ping the DRI on the blocked issue and/or link the blocking issue to raise visibility.

##### Implementation Approach

For engineers, you may want to create an implementation approach when moving an issue out of `~workflow::planning breakdown`. A proposed implementation approach isn't required to be followed, but is helpful to justify a recorded weight.

As the DRI for `workflow::planning breakdown`, consider following the example below to signal the end of your watch and the issues preparedness to move into scheduling. While more straightforward issues that have already been broken down may use a shorter format, the plan should (at a minimum) always justify the "why" behind an estimation.

The following is an example of an implementation approach from [https://gitlab.com/gitlab-org/gitlab/-/issues/247900#implementation-plan](https://gitlab.com/gitlab-org/gitlab/-/issues/247900#implementation-plan). It illustrates that the issue should likely be broken down into smaller sub-issues for each part of the work:

```md
### Implementation approach

~database

1. Add new `merge_requests_author_approval` column to `namespace_settings` table (The final table is TBD)

~"feature flag"

1. Create new `group_merge_request_approvers_rules` flag for everything to live behind

~backend

1. Add new field to `ee/app/services/ee/groups/update_service.rb:117`
1. Update `ee/app/services/ee/namespace_settings/update_service.rb` to support more than just one setting
1. *(if feature flag enabled)* Update the `Projects::CreateService` and `Groups::CreateService` to update newly created projects and sub-groups with the main groups setting
1. *(if feature flag enabled)* Update the Groups API to show the settings value
1. Tests tests and more tests :muscle:

~frontend

1. *(if feature flag enabled)* Add new `Merge request approvals` section to Groups general settings
1. Create new Vue app to render the contents of the section
1. Create new setting and submission process to save the value
1. Tests tests and more tests :muscle:
```

Once an issue has been estimated, it can then be moved to `workflow::scheduling` to be assigned a milestone before finally being `workflow::ready for development`.

#### Planning

We plan in monthly cycles in accordance with our [Product Development Timeline](/handbook/engineering/workflow/#product-development-timeline). While meeting this timeline is up to the discretion of individual groups, a typical planning cycle is suggested to look like:

* By the 4th, Product should have created a planning issue for their group in the [Manage project](https://gitlab.com/gitlab-org/manage/issues) for the coming release.
  * This issue should include a tentative plan for the release, along with links to boards that represent the proposed work for the milestone. Please see examples from [Fulfillment](https://gitlab.com/gitlab-org/fulfillment-meta/issues/37), [Manage](https://gitlab.com/gitlab-org/manage/issues/65), and [Create](https://gitlab.com/gitlab-org/create-stage/issues/33)!
  * Issues without estimates should have the `workflow::planning breakdown` label applied to make estimation easier and be marked for the coming release as `Deliverable`. Issues of particular significance to our stage's strategy should be marked with `direction`.
  * At this stage, the Product Manager applies the `quad-planning::ready` label to all `feature` issues and assigns them to the SET counterpart for the group.
  * To review the proposed scope and kick start estimation, synchronous meetings with Engineering and Design are recommended.
* By the 12th, all planned issues proposed for the next release should be estimated by engineering (`workflow:ready for development`).
  * To assist with capacity planning, we track the cumulative weight of closed issues over the past 3 releases on a rolling basis. The proposed scope of work for the next release should not exceed 80% of this to account for slippage from the previous release.
  * After estimation, Product should make any needed adjustments to the proposed scope based on estimates. A synchronous meeting to review the final release scope is recommended.
* By the 20th, Product should review the release that just concluded development (currently, we transition development work from one release to the next on the 18th) for issues that slipped from the milestone. Please evaluate issues that weren't merged in time and reschedule them appropriately.

#### During a release

* When an issue is introduced into a release after Kickoff, an equal amount of weight must be removed to account for the unplanned work.
* Development should not begin on an issue before it's been estimated and given a weight.

#### Proof-of-concept MRs

We strongly believe in [Iteration](/handbook/values/#iteration) and delivering value in small pieces. Iteration can be hard, especially when you lack product context or are working in a particularly risky/complex part of the codebase. If you are struggling to estimate an issue or determine whether it is feasible, it may be appropriate to first create a proof-of-concept MR. The goal of a proof-of-concept MR is to remove any major assumptions during planning and provide early feedback, therefore reducing risk from any future implementation.

* Create an MR, prefixed with `PoC: `.
* Explain what problem the PoC MR is trying to solve for in the MR description.
* Timebox it. Can you determine feasibility or a plan in less than 2-3 days?
* Identify a reviewer to provide feedback at the end of this period.
* Close the MR. Provide a summary in the original issue on what you learned from the PoC, including product and performance implications.
  * State whether you are able to move forwards with implementation or not.
  * Please do not close the issue.

The need for a proof-of-concept MR may signal that parts of our codebase or product have become overly complex. It's always worth discussing the MR as part of the retrospective so we can discuss how to avoid this step in future.

### Working on unscheduled issues

Everyone at GitLab has the freedom to manage their work as they see fit,
because [we measure results, not hours](/handbook/values/#measure-results-not-hours). Part of this is the
opportunity to work on items that aren't scheduled as part of the
regular monthly release. This is mostly a reiteration of items elsewhere
in the handbook, and it is here to make those explicit:

1. We expect people to be [managers of one](/handbook/values/#managers-of-one), and we [use
   GitLab ourselves](/handbook/values/#dogfooding). If you see something that you think
   is important, you can request for it to be scheduled, or you can
   [work on a proposal yourself](/handbook/values/#dont-wait), _as long as you keep your
   other priorities in mind_.
2. From time to time, there are events that GitLab team-members can participate
   in, like the [issue bash](https://about.gitlab.com/community/issue-bash/) and [content hack days](/handbook/marketing/corporate-marketing/content/content-hack-day/#content-hack-day). Anyone is welcome
   to participate in these.

When you pick something to work on, please:

1. Follow the standard workflow and assign it to yourself.
2. Share it in `#s_manage` to encourage [transparency](/handbook/values/#transparency)

#### Retrospectives

After the Kickoff, the Manage stage conducts an [asynchronous retrospective](/handbook/engineering/management/team-retrospectives/) on the prior release. You can find current and past retrospectives for Manage in [https://gitlab.com/gl-retrospectives/manage/issues/](https://gitlab.com/gl-retrospectives/manage/issues/).

We're currently exploring group-specific retrospectives, in order to create a private and safe environment for group members to share thoughts, feedback, and concerns amongst group members they work more often with.


### Group Access - additional considerations

#### Security Rota

The Access group are responsible for several product categories that attract a significant amount of scrutiny from a security perspective. An outcome of this is that we see a higher number of new security issues being created compared to other groups, which over time has caused the security backlog to grow. To help tackle the backlog, from milestone 13.3, we will assign 2 Backend Engineers per milestone to a security rota.

The goal of this new process is to achieve more consistent progress in [burning through the backlog](https://app.periscopedata.com/app/gitlab/695525/Manage::Access-Backend-Overview). Historically we have focussed on solving high severity (severity::1/severity::2) issues, while trying to maintain a pragmatic balance with other concerns in the team - but this hasn't been effective in reducing the overall backlog. By dedicating capacity each milestone we can separate the planning of security issues from the main milestone goals, with the aim of introducing a more optimal process.

Engineers who are part of the rota should work from [this board](https://gitlab.com/groups/gitlab-org/-/boards/1816615).

| Release | Start Date | First Engineer | Second Engineer |
|-----:|----------|---|
| 13.3 | 17th July 2020 | [mksionek](https://gitlab.com/mksionek) | [dblessing](https://gitlab.com/dblessing) |
| 13.4 | 17th August 2020 | [ifarkas](https://gitlab.com/ifarkas) | [manojmj](https://gitlab.com/manojmj)  |
| 13.5 | ~28th September 2020 | [alexpooley](https://gitlab.com/alexpooley) |  [sarcila](https://gitlab.com/sarcila) |
| 13.6 | ~26th October 2020 | [ifarkas](https://gitlab.com/ifarkas) | [manojmj](https://gitlab.com/manojmj) |
| 13.7 | ~23rd November 2020 | | |

**_Comment:_** working on multple issues that are focused on one aspect of the application reduces context switch, helps with gaining knowledge faster and allows to leave this part of the application in significantly better condition after one security rota cycle.

#### Refinement

To help with milestone planning, we encourage asynchronously weighing issues to help get them `ready for development`.

1. Engineering Manager will review the [Refinement Board](https://gitlab.com/groups/gitlab-org/-/boards/1747837) on a weekly basis.
1. Each Engineer is assigned a few issues per week from the board to be weighed as per the [estimation](#estimation) process. Where possible, the people closest to the problem will be assigned.
1. The Engineer assesses issue to determine if it is `workflow:ready for development` or not. Clarifying questions are asked on the issue. Product Manager adjusts description and scope as needed. This process may take a few days and the objective is to have an issue defined well enough to be estimated.
1. Once all questions are answered and the issue description is updated, Engineering assigns a weight to the issue.
1. To encourage collaboration, the Engineer will pick someone else from the team (at random) to review the discussion.
1. If the reviewer is happy with the weight and scope they will set the `workflow:ready for development` label. Alternatively they should raise their concerns in the issue. If the proposal is changed, the issue description should be updated.

### Group Import - additional considerations

While group Import follows the Manage's process described above, we utilize additional flows that help us better prepare issues for scheduling into milestones.

#### Issue Triage

In order to assign an [incoming issue](/handbook/engineering/quality/triage-operations/#triage-reports) to a milestone or close it, the product manager may require input from the engineers.

To facilitate an asynchronous triage question and answer flow, the issue is labeled with the `triage question` label and the team members are invited into a thread where the question is discussed. Any team member can answer the question and mention the product manager in the answer. If the product manager has enough information to triage the issue, the `triage question` label is removed and the issue is assigned to the appropriate milestone or closed as infeasible.

#### Issue Refinement

For an issue to be deemed `workflow::ready for development`, the issue goes through a refinement stage, where the engineers, the product manager, and other stable counterparts are able to evaluate the issue, ask any clarifying questions, and discuss potential approaches. At the end of that conversation, the issue can be deemed `workflow::ready for development`, marked for further breakdown or even canceled if determined not feasible. As the team creates context around the issue, they also estimate the weight during this phase.

All issues considered for the next 1-3 milestones need to go through the refinement, in order to be ready for scheduling into a specific release. Refining issues that are being considered for releases further in the future than the next 3 milestones is not recommended, as it may lead to throw-away effort.

To place an issue into refinement, the issue is labeled with `workflow::refinement`. A comment thread is created in the issue to invite the team members to evaluate the issue asynchronously and vote (using emojis) whether the issue is ready for development or not. Any "no" votes are explained and further discussed in comments until the vote becomes a "yes", or the issue is removed from consideration.

Emojis are also used to vote for the issue weight. Any outlier votes are discussed in comments until a compromise is reached. Large estimates are also discussed to determine if the issue can be broken down further.

If the issue has all "yes" votes and has a weight estimate, it is deemed `workflow::ready for development`.

Prior to the planning meeting, a milestone priority is assigned to each issue. To avoid collisions of the milestone priorities with the priority::1-priority::4 bug and security priorities, we use the `milestone::p1`-`milestone::p4` labels. This process is experimental and will be finalized once we are able to evaluate and discuss the usefulness of these labels.

## Meetings

Although we have a bias for asynchronous communication, synchronous meetings are necessary and should adhere to our [communication guidelines](/handbook/communication/#video-calls). Some regular meetings that take place in Manage are:

| Frequency | Meeting                              | DRI         | Possible topics                                                                                        |
|-----------|--------------------------------------|-------------|--------------------------------------------------------------------------------------------------------|
| Weekly    | Group-level meeting                  | Backend Engineering Managers | Ensure current release is on track by walking the board, unblock specific issues                       |
| Every 2 weeks | Stage-level Product/UX           | @jeremy          | Collaborate on WIP design, brainstorm together on specific problems                                    |
| Every 2 weeks | Stage-level Product/Engineering  | @jeremy          | Collaborate across groups on specific issues, discuss global stage-level optimization (process, people)   |
| Monthly   | Stage-level meeting (Manage Monthly) | @jeremy     | Stage-level news, objectives, accomplishments                                                          |
| Monthly   | Stage-level social call              | @dennis     | Getting to know each other                                                                             |
| Monthly   | Planning meetings                    | Product Managers         | See [Planning](/handbook/engineering/development/dev/manage/#planning) section |

For one-off, topic specific meetings, please always consider recording these calls and sharing them (or taking notes in a [publicly available document](https://docs.google.com/document/d/1kE8udlwjAiMjZW4p1yARUPNmBgHYReK4Ks5xOJW6Tdw/edit)).

Meetings that are not 1:1s or covering confidential topics should be added to the Manage Shared calendar.

All meetings should have an agenda prepared at least 12 hours in advance. If this is not the case, you are not obligated to attend the meeting. Consider meetings canceled if they do not have an agenda by the start time of the meeting.

### Shared calendar

* To add the shared calendar to your Google Calendar, please use this [link](https://calendar.google.com/calendar/b/1?cid=Z2l0bGFiLmNvbV9rOWYyN2lqamExaGoxNzZvbmNuMWU4cXF2a0Bncm91cC5jYWxlbmRhci5nb29nbGUuY29t) (GitLab internal).
* To add a meeting to the shared calendar, please add [the link in this document](https://docs.google.com/document/d/1IxGuORI-vfVd6irNdUwpnOBZDWALWzOqhQzC9E39ixQ/edit) to the event.
* To add a new member to the shared calendar
  * Click "Settings and Sharing" in the kebab menu when mousing over "Manage Shared" in your Google Calendar sidebar under "My calendars".
  * Scroll to the "Share with specific people" section of the settings area. Click "Add people" and add the new member with "Make changes and manage sharing".
* For a more detailed walkthrough, have a look at a quick [video walkthrough](https://www.youtube.com/watch?v=TmcPuuljf1w)

## Stage Members

The following people are permanent members of the Manage stage:

<%= stable_counterparts(role_regexp: /[,&] Manage/, direct_manager_role: 'Backend Engineering Manager, Manage') %>

### Links and resources
{: #links}

* Our repository
  * A number of our stage discussions and issues are located at [`gitlab-org/manage`](https://gitlab.com/gitlab-org/manage/)
* Our Slack channels
  * Manage-wide [#s_manage](https://gitlab.slack.com/messages/CBFCUM0RX)
  * Manage:Access [#g_manage_access](https://gitlab.slack.com/messages/CLM1D8QR0)
  * Manage:Analytics [#g_manage_analytics](https://gitlab.slack.com/messages/CJZR6KPB4)
  * Manage:Compliance [#g_manage_compliance](https://gitlab.slack.com/messages/CN7C8029H)
  * Manage:Import [#g_manage_import](https://gitlab.slack.com/messages/CLX7WMSKW)
* Meeting agendas
  * Agendas and notes from stage meetings can be found in [this Google Doc](https://docs.google.com/document/d/1kE8udlwjAiMjZW4p1yARUPNmBgHYReK4Ks5xOJW6Tdw/edit). For transparency across the stage, we use one agenda document.
* Recorded meetings
  * Recordings should be added to YouTube and added to [this playlist](https://www.youtube.com/playlist?list=PLFGfElNsQthZ-D0khZ_NSb5Bdl2xkF97m).

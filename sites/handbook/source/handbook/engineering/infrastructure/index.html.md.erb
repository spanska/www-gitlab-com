---
layout: handbook-page-toc
title: "Infrastructure"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}


## Mission

The **Infrastructure Department** is responsible for the **availability**, **reliability**, **performance**, and **scalability** of GitLab.com and other supporting services. These responsibilities have cost efficiency as an additional driving force, reinforced by the properly prioritized [**dogfooding**](#dogfooding) efforts.
Many other departments and teams also contribute to the success of our service. It is the responsibility of the Infrastructure Department to drive ongoing accountability by closing the feedback loop with monitoring and metrics.

## Vision

We are a blend of operations gearheads and software crafters whose highest priority is the protection of the integrity and reliability of the operational environments that support GitLab.com. We apply sound engineering principles, a healthy dose of operational discipline, and the selection of technologies to build mature automation that safeguards the operational environment while driving changes through it. We aim to make GitLab.com ready for mission-critical customer workloads, and we strive for excellence every day by living and breathing [**GitLab's values**](/handbook/values/) as our guiding operating principles in every decision we make and every action we take.

## Strategy

The Infrastructure Department work towards completing the vision for enterprise grade ready GitLab SaaS platform is conducted in a number of ways. Through writing up [Designs](#design), setting quarterly OKR's but also by driving projects that span multiple quarters.

One of the strategic initiatives that is driving us towards the vision, aligned with [the company direction strategy](/direction/index.html#strategic-response), is [GitLab.com running on the Kubernetes platform](/handbook/engineering/infrastructure/production/kubernetes/gitlab-com/).

Other strategic initiatives to achieve this vision are driven by the needs of enterprise customers looking to adopt GitLab.com. [The GitLab.com strategy](https://about.gitlab.com/direction/enablement/dotcom/) catalogs top customer requests for the SaaS offering and outlines strategic initiatves across both Infrastructure and Stage Groups needed to address these gaps.

The strategy section will expand to add more details in the future.

## Design

The [**Infrastructure Library**][library] contains documents that outline our thinking about the problems we are solving and represents the ***current state*** for any topic, playing a significant role in how we produce technical solutions to meet the challenges we face.

**Blueprints** scope out our initial thinking about specific problems and issues we are working on. **Designs** outline the specific architecture and implementation.

## Dogfooding

The Infrastructure department uses GitLab and GitLab features extensively as the main tool for operating many [environments](/handbook/engineering/infrastructure/environments/), including GitLab.com.

We follow the same [dogfooding process](/handbook/engineering/#dogfooding) as part of the Engineering function, while keeping the [department mission statement](#mission) as the primary prioritization driver. The prioritization process is aligned to [the Engineering function level prioritization process](/handbook/engineering/#prioritizing-technical-decisions) which defines where the priority of dogfooding lies with regards to other technical decisions the Infrastructure department makes.

## OKRs

GitLab uses **Objectives and Key Results** (OKRs) as *[quarterly goals](/company/okrs/) to execute our strategy to make sure [said] goals are clearly defined and aligned throughout the organization*. We capture the objectives in [epics](https://gitlab.com/groups/gitlab-com/gl-infra/-/epics?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=okr%3A%3Ao).

## Issue Tracker

The [infrastructure issue tracker](https://gitlab.com/gitlab-com/gl-infra/infrastructure/issues) is the backlog for the infrastructure team and tracks all the work our [teams](/teams) are doing–unrelated to an ongoing change or incident.

## Projects

Classicfication of the Infrastructure department projects is described on the [infrastructure department projects page](/handbook/engineering/infrastructure/projects).

## Meetings

GitLab is a widely distributed company, and we aim to work asynchronously most of the time. There are times, however, when we must get *together* to discuss topics in real time, and thus, we do have some meetings scheduled. Meetings start on time and end on time&mdash;or earlier.

Our team [calendar](https://calendar.google.com/calendar?cid=Z2l0bGFiLmNvbV9vamk2ZGtpMWZyYzhnOHFxOWZldXUxanRkMEBncm91cC5jYWxlbmRhci5nb29nbGUuY29t) is shared with the company.

Every scheduled team meeting _must_ have a Google Doc agenda attached to the invite. The agendas should be long-running and organized by date. Each meeting's agenda is set and reviewed before the start of each meeting. Everyone invited to the meeting should have edit rights to add agenda items before the start of the meeting and to take notes during the meeting.

##### GitLab SaaS Infrastructure

The GitLab SaaS Infrastructure meeting is one of many channels to share and distribute Infrastructure-related information relevant to the entire company. The meeting is organized by the Infrastructure leadership team and the VP of Infrastructure. This meeting aims to bring together many different related aspects which all influence the GitLab SaaS infrastructure reliability, scalability, performance, and efficiency.

This meeting is always to be recorded and made available in support of those in varying timezones. It is meant to be a learning environment for what is coming, what we've done well, and what we can do to improve. All GitLab team members are welcome to attend this meeting.

While the agenda will continually evolve, the main structure is outlined below. Everyone should feel welcome to contribute to the agenda, but [Infrastructure mStaff](/handbook/engineering/infrastructure#mstaff) will work to curate the agenda into the best use of 50 minutes each week.
* Core Topics
   * Availability:  Update on recent week performance, or look back at past month.
   * Recent severity::1/severity::2 Incident Summary: Quick overview to bring attention to recent critical service impacts.
   * Infrastructure related Development escalations: Quick status on [infradev](https://gitlab.com/groups/gitlab-org/-/boards/1193197?label_name[]=infradev) board, triage Open issues if needed.
   * Spend Efficiency: Update on recent performance, quick highlight of important current work.
* Saturation and Capacity
   * Saturation dashboard view
   * Rotating deep dive on specific part of infrastructure saturation or capacity projections
* Wins / Misses / Stories: Highlight key areas where we can celebrate and/or learn.
* Rotating Topics
   * Corrective Action item health: Review of how we're addressing the corrective action backlog [Monthly]
   * Product Limits activity update: Look at how we're progressing in adding and improving useful application limits [Monthly]
   * Key Customer insights: Gain understanding of how customers are using us, learn about key customers [Varies]
   * Interesting project or feature highlight [Varies]

The recording of this meeting will be made available on GitLab Unfiltered.

##### Design and Automation (DNA)

Design and Automation (DNA) is a **purely technical meeting** for Infrastructure ICs to discuss technical topics. The agenda is driven by design documents from the [library], although discussion on other technically relevant topics is welcome. Project status discussions are strictly out of bounds in this (the only exception being the resolution of technical dependencies).

While open discussions are welcome, it is strongly recommended that blueprints and designs are used as the source of agenda items. This allows everyone gain the required context–before the meeting starts–for an engaging conversation.

During discussions, it is ok to point shortcomings for a given design. This is one way in which we expand our angle of vision and learn. In general, however, **make it a point to provide alternatives**.

##### Staff Meetings

Each team in Infrastructure has a weekly Staff meeting, where relevant team issues are discussed. These meetings are organized by Infrastructure Managers for their respective teams.

##### mStaff

Infrastructure mStaff is a loose denomination for the group of people who report directly to the Vice President of Infrastructure. This is a group composed of both managers and individual contributors, and they are responsible for the overall direction of Infrastructure and the achievement of our goals:

<%= direct_team(manager_role: 'VP of Infrastructure')%>

The weekly mStaff brings together Infrastructure's management team for a weekly sync to prepare for the week and address issues that require attention. The meeting is organized by the VP of Infrastructure.

## Teams

The Infrastructure Department is comprised of three distinct groups:

* [**Reliability** teams](/handbook/engineering/infrastructure/team/reliability/), which operate all user-facing GitLab services.
* [**Delivery**](/handbook/engineering/infrastructure/team/delivery/), which focuses on GitLab's delivery of software releases to GitLab.com and the public at large.
* [**Scalability**](/handbook/engineering/infrastructure/team/scalability/), which focuses on improving GitLab application at GitLab.com scale.

For details on the Department's structure, see the [**Infrastructure Teams Handbook section**](/handbook/engineering/infrastructure/team/).

<%= partial "handbook/engineering/infrastructure/_common_links.html" %>

[library]: https://gitlab.com/gitlab-com/gl-infra/readiness/-/tree/master/library

---
layout: handbook-page-toc
title: "Mapping Jobs to be Done (JTBD)"
---

#### On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

##  Mapping Jobs to be Done (JTBD)

### How do I create a job map?
The main job is a process that you can map chronologically. The map reveals the process of completing the job from the executor’s point of view, not the buyer or customer perspective. The intent is to illustrate what the job performer is striving to get done at each stage in executing a job.
A job map is not a customer journey, service blueprint, or workflow diagram. It does not reflect what a person does to discover, learn about, select, buy, and use a product or service. These activities are relevant to the buyer and purchasing process.

Note: It’s not about mapping tasks or physical activities, but about creating a sequence of smaller goals that make up the main job. Ideally, the job map will not include any means of performing the job.

From your interviews, create a sequence of stages in a visual representation that show underlying patterns of intent. As you map, think about what job performer’s subgoals are and what are the phases of intent that unfold as the job gets done. There is a [template in Mural ](https://app.mural.co/template/a8de4f06-b402-45ed-8eb2-bc17cf567b5e/0444b92d-148e-46b7-86cc-83a7b4a15d6b)that you can use for this. The template uses the eight standard phases from Tony Ulwick’s Outcome-Driven Innovation (ODI) method:
1. **Define:** Determine objectives and plan how to get the job done. 
1. **Locate:** Gather materials and information needed to do the job.
1. **Prepare:** Organize materials and create the right setup.
1. **Confirm:** Ensure that everything is ready to perform the job.
1. **Execute:** Perform the job as planned.
1. **Monitor:** Evaluate success as the job is executed.
1. **Modify:** Modify and iterate as necessary.
1. **Conclude:** End the job and follow-up. 

Each stage should have a purpose and be formulated as a functional job. Avoid including emotional and social aspects in the stage labels, and avoid bringing in adjectives and qualifiers that indicate a need, like “quickly” or “accurately.” Strive to make the stages as universal and stable as possible without reference to the means of execution. Jobs are separate from solutions. 

Consider these stages as more of a checklist than a prescriptive model. The point is to remember to cover all types of stages involved in executing the main job—before, during, and after. You’ll have to modify their labels as needed to describe your particular main job. Keep the labels short, ideally expressed as single-word verbs. The list below reflects some common verbs for each of the stage types in the universal job map. 
1. **Define:** Define, Plan, Select, Determine.
1. **Locate:** Locate, Gather, Access, Retrieve.
1. **Prepare:** Prepare, Set up, Organize, Examine.
1. **Confirm:** Confirm, Validate, Prioritize, Decide.
1. **Execute:** Execute, Perform, Transact, Administer.
1. **Monitor:** Monitor, Verify, Track, Check.
1. **Modify:** Modify, Update, Adjust, Maintain.
1. **Conclude:** Conclude, Store, Finish, Close.

**Step 1 - Create a job map ([Mural Template](https://app.mural.co/template/a8de4f06-b402-45ed-8eb2-bc17cf567b5e/0444b92d-148e-46b7-86cc-83a7b4a15d6b))**
1. Start with the three large phases of the main job: beginning, middle, and end. Arrange the micro-jobs uncovered from your interviews in the appropriate category.
1. Continue to group the jobs into stages, using the universal stages as a starting point, but changing the labels as needed. Language is important, so spend time refining the labels and divisions as you go. 

You might end up with fewer or more stages than eight.
It’s also possible to include a loop for interaction or even a branch in the flow. The diagram you create should stand as a clear model for describing the process of performing the job that everyone in your organization can relate to.

Ideally, you will validate this job map with job performers. Talk through it with them. If the labels and/or divisions between stages need a great deal of explanation or seem to be confusing, rework them until it’s simple enough to be self explanatory.

**Step 2 - Put the job map to use**
Job maps can be used to identify opportunities and ways to create new value. The job map ultimately defines the scope of your business/stage group. Align your solutions to it to spot gaps and opportunities. Compare alternative offerings and means of getting a job done for competitive insight. Prioritize areas within the job process to drive your service roadmap. Find opportunities that can be reflected in marketing campaigns and sales pitches. 

To get started, ask yourself these questions:
* Is there a more efficient order of stages in performing the job?
* Where do people struggle the most to get the job done?
* What causes the job to get off track? 
* Can you eliminate stages or steps along the way?
* How might the job be carried out in the future, given current trends?
* How might you get more of the job done for customers? 
* What related jobs can your offering address or tie in to the job?

## How do I validate a JTBD?
If we do not have a high level of confidence (supported by research) in our JTBD, we want to make sure we are matching our users’ perspective and that we are prioritizing JTBD that matter to them. 

This can be done via interviews or a quantitative survey.

*  Create a survey in Qualtrics that allows participants to:
    * Stack rank the JTBD relevant to their day to day work
    * Evaluate the maturity (based on GitLab’s definition) of the top 2 JTBD they chose 
    * Provide tasks for those 2 JTBD so they can rate each on how they impact their job
    * Open ended question for the 2 JTBD to uncover anything missed
    * Review the data with lenses on the user segments/personas. Ask yourself if the JTBD data is different across them? What other differences do you see?
    
*  Conduct interviews with job performers and touch on the themes of the JTBD. Pay attention to whether the need statement resonates and applies to them or not.

Follow additional guidance on [how to validate JTBD](/handbook/engineering/ux/jobs-to-be-done/validating-jobs-to-be-done/).

## References and Further Reading
- [Jobs to be Done Playbook by Jim Kalbach](https://rosenfeldmedia.com/books/jobs-to-be-done-book/)
- [Jobs to be Done by Anthony Ulwick](https://jobs-to-be-done-book.com/)
- [Competing Against Luck by Clay Christenson](https://www.amazon.com/dp/0062435612/ref=cm_sw_em_r_mt_dp_U_v0k9Eb92AEDZX)
- [Intercom on Jobs to be Done by Intercom](https://www.intercom.com/resources/books/intercom-jobs-to-be-done)

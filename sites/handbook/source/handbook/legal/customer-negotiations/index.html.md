---
layout: handbook-page-toc
title: "Sales Guide: Collaborating with GitLab Legal"
---

## OVERVIEW 
### 🎥 [Watch Overview Video](https://www.youtube.com/watch?time_continue=2&v=snb-1ENQitI&feature=emb_logo) 
*Learn about the purpose of this resource; 1:40 (Log into GitLab Unfiltered)*

Thank you for visiting! The purpose of this resource is to provide Sales reps assistance on: 
    
(1) [Operational](https://about.gitlab.com/handbook/legal/customer-negotiations/#operational) - How to work with, and engage, GitLab legal; Requirements to close a deal; General FAQ for legal ops, including some parallels to Finance and Deal Desk matters. 

(2) [Educational](https://about.gitlab.com/handbook/legal/customer-negotiations/#educational) - Learning about how and why GitLab legal and our agreements work, to better serve you during a sales cycle; Proactive advice and education to enable you to feel more confident when / if your Customer has legal related questions.   


## ENGAGEMENT
- As part of this collaboration, we are measuring success by interaction with the field. For this reason, we ask that you **please watch the videos and "thumbs-up" or "like" them.**
- **Please Note**: All videos provided will be up on the GitLab Unfiltered YouTube Account. You will need to login to this Account in order to view. 

## REQUESTING CONTENT
- You are always encouraged to make requests for future content that will help you and the team. Simply complete the form [here](https://forms.gle/2zznmLFznSqJAQUH6).
  

| OPERATIONAL| EDUCATIONAL |
| ------ | ------ |
| [**How to reach Legal** ](https://about.gitlab.com/handbook/legal/customer-negotiations/#how-to-reach-legal)                  | [**Overview of GitLab Agreements**](https://about.gitlab.com/handbook/legal/customer-negotiations/#Overview-of-GitLab-Agreements)  |
| [**How do I get an Agreement signed**](https://about.gitlab.com/handbook/legal/customer-negotiations/#how-do-I-get-an-Agreement-signed)       | [**When does GitLab negotiate**](https://about.gitlab.com/handbook/legal/customer-negotiations/#When-does-GitLab-Negotiate)  |
| [**Clearing Export Review in SFDC** ](https://about.gitlab.com/handbook/legal/customer-negotiations/#clearing-export-review-negotiations)     | *COMING SOON: Introduction to Open Source* |
|[**Completing Vendor Request Forms** ](https://about.gitlab.com/handbook/legal/customer-negotiations/#Completing-Vendor-Request-Forms) | *COMING SOON: Commonly negotiated elements* |
| *COMING SOON: How Partner's can execute a Partner Agreement*      | *COMING SOON: What is Intellectual Property?* |


## OPERATIONAL

## How to reach Legal 
### 🎥 [Watch How to Reach Legal Video](https://www.youtube.com/embed/bCLagn_sWSI)
*Learn about how to open a Legal Request, When to Open a Legal Request; 2:24 (Log into GitLab Unfiltered)*

1. For all questions related to Partners and/or Customers, including: NDA's, Contract Review / Negotiations, Legal Questions...etc., please open a **Legal Request** in SFDC. 
2. Please note: There is a $25K opportunity minimum threshold to review edits to our terms, and a $100K opportunity minimum threshold to review Customer / Partner Terms. These thresholds do not apply to NDA's. 
3. Step-by-Step directions on opening a Legal Request can be found [here](https://about.gitlab.com/handbook/sales/field-operations/order-processing/#request-editable-version-of-gitlab-template)
4. A presentation on this topic can be found [here](https://docs.google.com/presentation/d/1lesWNvPAFd1B3RuCgKsqQlE85ZEwLuE01QpVAKPhQKw/edit#slide=id.g5d6196cc9d_2_0)

**Additional Resources:**
[Field Operations Legal Request Overview](https://about.gitlab.com/handbook/sales/field-operations/order-processing/#contact-legal) 

## How do I get an Agreement signed
### 🎥 [Watch How do I get an Agreement Video](https://www.youtube.com/embed/FzD3j0RI8do)
*Learn about how to get an Agreement Signed; Who can sign on behalf of GitLab; 3:40 (Log into GitLab Unfiltered)*

1. **Sales Reps cannot sign any Agreements.** Only certain individuals may execute on behalf of GitLab see Authorization Matrix [here](https://about.gitlab.com/handbook/finance/authorization-matrix/#authorization-matrix). 
2. All Agreements require a GitLab Legal stamp in order to be executed. This stamp is placed by a Contract Manager when an executable version is reached. 
3. Electronic signature requests can be generated from Sertifi in SFDC, OR, HelloSign. 

**Additional Resources:**
[Field Operations Signature Process Overview](/handbook/sales/field-operations/order-processing/#how-to-obtain-signatures-for-any-external-contract-or-agreement-including-vendor-forms) 

## Clear Export Review in SFDC
1. See the [Visual Compliance Overview](https://docs.google.com/presentation/d/1hD1xgEyJL1U1eyvTci6NNlCdIur4PkB16GO52wjZRgk/edit#slide=id.g5dbc2f529a_0_41)
2. GitLab uses Visual Compliance to review all Accounts. The tool is continually reviewing and 'clearing' Accounts. In addition, any 'flags' / 'hits' are reviewed and cleared throughout the day by GitLab Legal.
3. If an export notice is presented, that requires immediate action, please tag "@legal" in SFDC Chatter for the Account and write "Please review for export compliance"
4. Legal will continually review all requests. Once cleared in Visual Compliance update(s) will reaching SFDC in 15-20 minutes. 

## Completing Vendor Request Forms

**It is the sales team member responsibility to complete Vendor Request Forms**
1. Much, if not all, of the Vendor Form information can be found at the [Company Information Page](https://gitlab.com/gitlab-com/Finance-Division/finance/-/wikis/company-information)

**Included on the Company Information Page is:**
- Address / information related to each GitLab entity
- All banking information
- ECCN Number
- NAICS Code
- Dun and Bradstreet Number
...AND VARIOUS OTHER DETAILS
- W9 is located on the Finance [Forms page](https://about.gitlab.com/handbook/finance/#forms)
- Request Insurance Certificate on [Legal Team Page](https://about.gitlab.com/handbook/legal/#4-requests-for-insurance-certificate)

2.  If there are questions / elements that are not found in the Company Information Page, the Sales Rep should engage Deal Desk to organize requests of finance and tax. Information regarding contacting Deal Desk and overall process can be found at the [Sales Order Process Page](https://about.gitlab.com/handbook/sales/field-operations/order-processing/#how-to-process-customer-requested-vendor-setup-forms)

3. **ONLY OPEN A LEGAL REQUEST IF:** There is a specific legal question / element being requested within the Vendor Form.

Example: Has GitLab, or board member / executed of GitLab, been subject to a lawsuit? 

 
**As a private company, we will not share the following information in a Vendor Request Form:** 
- Stock / Share ownership of the company
- Finances, including annual and/or historical performance 


## EDUCATIONAL

## Overview of GitLab Agreements
### 🎥 [Watch Overview of GitLab Agreements Video](https://www.youtube.com/embed/M3ktHGDBebw)
*Learn about what Agreements GitLab has; 2:45 (Log into GitLab Unfiltered)*

1. GitLab provides our Software pursuant to the GitLab Subscription Agreement, and Professional Services pursuant to GitLab Professional Services Terms;
2. GitLab has a Master Partner Agreement that includes multiple Exhibits to enable Partners to, (i) Reseller, (ii) Refer, and (iii) Distribute GitLab Products and Services;
3. Other than web-portal purchases, GitLab enters into an Order Form with a Customer which outlines the Products and Services being sold;
4. Any modification(s) to Order Forms, or standard Terms and Conditions, require GitLab Legal Approval. 

## When does GitLab negotiate
### 🎥 [Watch When does GitLab Negotiate Video](https://www.youtube.com/embed/zYvZKP5OohA)
*Learn about when GitLab negotiates; Where to find an NDA template; 3:20 (Log into GitLab Unfiltered)*

1. GitLab will negotiate NDAs with our prospects. You can always send the prospect the GitLab NDA template found [here](/handbook/legal/#contract-templates) and ask for them to sign. If they want to use their template, simply open a Legal Request for "Contract Review".
2. GitLab will negotiate changes to our terms and conditions provided the [current] Opportunity is greater than $25K (USD).
3. GitLab will negotiate using a Customer terms and conditions-if they are applicable to our Software and business-if the [current] Opportunity is greater than $100K (USD).
4. **Why don't we negotiate with all Customers?** Using resources to negotiate on all deals would not be a good use of GitLab resources and instead, we created our terms to be reasonable and in line with industry standards which allows us to place our resources into creating better products and services. 

**Additional Resources**
[Field Operations Negotiation Overview](/handbook/sales/field-operations/order-processing/#using-customer-form-agreements-and-negotiating-gitlabs-standard-agreement)


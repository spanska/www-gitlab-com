---
layout: handbook-page-toc
title: Product Team Performance Indicators
---
## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

[**Principles**](/handbook/product/product-principles) - [**Processes**](/handbook/product/product-processes) - [**Categorization**](/handbook/product/product-categories) - [**GitLab the Product**](/handbook/product/gitlab-the-product) - [**Being a PM**](/handbook/product/product-manager-role) - [**Performance Indicators**](/handbook/product/performance-indicators/) - [**Leadership**](/handbook/product/product-leadership/)

## Structure

In order to provide a useful single metric for [product groups](https://about.gitlab.com/company/team/structure/#product-groups) which maps well to product-wide Key Performance indicators, some product performance indicators cascade upwards. Here is how:

1. [Action Monthly Active Users (AMAU)](#action-monthly-active-users-amau) - Are the building blocks, these are unique users performing a certain action. _For example_ - unique users interacting with a [feature flag](https://docs.gitlab.com/ee/user/project/operations/feature_flags.html)
1. [Group Monthly Active Users (GMAU)](#group-monthly-active-users-gmau) - Are used for targeting the R&D efforts of [Product Groups](/company/team/structure/#product-groups). These are the maximum from the set of the group's AMAUs. _For example_ - In the [Progressive Delivery group](/handbook/product/product-categories/#progressive-delivery-group), if [feature flag](https://docs.gitlab.com/ee/user/project/operations/feature_flags.html) AMAU was 9,000 users and [review apps](https://docs.gitlab.com/ee/ci/review_apps/) AMAU was 11,000 then the Progressive Delivery GMAU would be 11,000.
1. [Stage Monthly Active users (SMAU)](#stage-monthly-active-users-smau) - Are used, in part, for targeting [R&D investments](/handbook/product/investment/#investment-by-stage) across multiple stages. These are the maximum from the set of the relevant GMAU's plus any stage level AMAU's that might not fall cleanly into a GMAU. _For example_ - In the [Release stage](/handbook/product/product-categories/#release-stage), if Release:Progressive Delivery GMAU is 11,000 and Release:Release Management GMAU is 8,000 and the Release-wide AMAU of unique users performing a deployment is 20,000 then the Release SMAU is 20,000.
1. [Section Monthly Active users (Section MAU)](#section-monthly-active-users-smau) - Are used, in part, for targeting [R&D investments](/handbook/product/investment/#investment-by-stage) across multiple sections. These are the maximum from the set of the relevant SMAU's plus any section level AMAU's that might not fall cleanly into a SMAU. _For example_ - The [Secure and Defend section](/handbook/product/secure-and-defend-section-performance-indicators), has opted to use an AMAU of [Unique users who have used a Secure scanner](https://about.gitlab.com/handbook/product/secure-and-defend-section-performance-indicators/#secure-and-defend-section---section-mau---unique-users-who-have-used-a-secure-scanner) to represent usage in their section.
1. [Section Total Monthly Active Users (Section TMAU)](#section-total-monthly-active-users-section-tmau) - Is used to highlight stage expansion across a section by summing the SMAUs across a section. Unlike other MAU metrics, each user will be counted multiple times.
1. [Total Monthly Active Users (TMAU)](#total-monthly-active-users-tmau) - Is used to highlight stage expansion by summing the SMAUs across all stages. Unlike other MAU metrics, each user will be counted multiple times. This is ok, as the metric is intended to capture the number of [Unique Monthly Active Users](https://about.gitlab.com/handbook/product/performance-indicators/#unique-monthly-active-users-umau) multiplied by [Stages per User](https://about.gitlab.com/handbook/product/performance-indicators/#stages-per-user-spu).
1. [Unique Monthly Active Users (UMAU)](#unique-monthly-active-users-umau) - Are unique users across the entire product.

_Notes:_

- For GMAU and SMAU there is a desire for these metrics to be the unique users across the aggregate of associated AMAU's but current limitations in how we receive usage ping data (already aggregated per event) means that is not currently possible. There is work to [create de-duplicated GMAU and SMAU metrics in Usage Ping underway](https://gitlab.com/gitlab-org/telemetry/-/issues/421).

### Non-MAU Performance Indicators

1. Primary Performance Indicator (PPI) - For some sections, stages, or groups, a MAU-style metric may not be their primary performance indicator (PPI). In these scenarios, a different metric will be marked as the PPI to indicate it is the primary metric.

### Three Versions of xMAU

xMAU is a single term to capture the various levels at which we capture Monthly Active Usage (MAU), encompassing Action (AMAU), Group (GMAU), Stage (SMAU), and Total (TMAU).  If you can use GMAU instead of xMAU please do so because it is more specific, more actionable, and a leading indicator. We should not give xMAU metrics without clearly indicating one of the three versions, Reported, Estimated, or Projected.

Each xMAU metric should have three versions:

1. Reported = reported .com xMAU + reported self-managed xMAU
1. Estimated = reported .com xMAU + estimated self-managed xMAU. The purpose of the estimated metric is to account for the fact that only part of self-managed instances report back xMAU data. The estimation is methodology is described on the [is it any good page](https://about.gitlab.com/is-it-any-good/#gitlab-is-the-most-used-devops-tool)
1. Projected = (reported .com xMAU + estimated self-managed xMAU) * (1 + current MoM growth rate * 36 months). The purpose of the projected metric is to gain a sense for what usage could look like in 3 years at current growth rates.

<%= performance_indicators('Product') %>

## PI Workflow

The PI workflow was presented in the Weekly Product Meeting on August 11, 2020. For a recap of this, please see the [slide deck](https://docs.google.com/presentation/d/1wCpvdCUtBtU4Y1vHDOSOLjhrlPvdYBAbydj58SRN5Js/edit) and [watch the video presentation](https://youtu.be/1l1ru7k-q2I?t=375).

### Objective

The objective of the PI workflow is to help the product organization achieve the Product Organizations FY21-Q3 OKRs:

- [100% of groups have Future GMAU (or Paid GMAU) with instrumentation, dashboards, and quarterly targets](https://gitlab.com/gitlab-com/Product/-/issues/1342)
- [100% of stages have Future SMAU and Paid SMAU with instrumentation, dashboards, and quarterly targets](https://gitlab.com/gitlab-com/Product/-/issues/1343).

To achieve this, we will be taking the following approach:

1. Get a XMAU in place for your section, stage, or group ASAP using [currently available tooling](https://docs.gitlab.com/ee/development/telemetry/).
1. If blocked, meet with [@jeromezng](https://gitlab.com/jeromezng) [@kathleentam](https://gitlab.com/kathleentam) to find work around solutions for your XMAU.
1. Improve your XMAU in future iterations as the collection framework improves.

To track the stataus of this OKR, we'll temporarily be using the [XMAU Tracking Sheet](https://docs.google.com/spreadsheets/d/18er-ZpqdtZDhs_bmZvJaU_iVsa3R38VMiau4C6RoPPs/edit#gid=706608817). This sheet is a temporary measure and we have an [epic](https://gitlab.com/groups/gitlab-com/-/epics/906) that's currently in progress to move this into the Product PI handbook page.

Note: XMAU is used to represent all the variations of AMAU, GMAU, Paid GMAU, SMAU, Paid SMAU, Section MAU, and Paid Section MAU.

### Overview

The PI workflow outlines the steps required for putting a product performance indicator in place.

| Step | Description | Responsibility |
| ---- | ----------- | -------------- |
| [Privacy Policy](https://about.gitlab.com/privacy/) | The privacy policy step describes what product usage data we can collect from our users. | Product Analytics Responsibility |
| [Collection Framework](https://about.gitlab.com/direction/telemetry/#build-collection-framework) | The collection framework step describes our available tooling for collecting product usage data. | Product Analytics Responsibility |
| [Event Dictionary](https://docs.google.com/spreadsheets/d/1VzE8R72Px_Y_LlE3Z05LxUlG_dumWe3vl-HeUo70TPw/edit?usp=sharing) | The event dictionary step outlines our single source of truth of the product metrics and events we collect. | PM Responsibility, Product Analytics Support |
| [Instrument Tracking](https://docs.gitlab.com/ee/development/telemetry/usage_ping.html#implementing-usage-ping) | The instrumentation step outlines how each product team implements data collection. | PM Responsibility, Product Analytics Support |
| [Data Availability](https://about.gitlab.com/upcoming-releases/) | The data availability step outlines the timing of a product release to receiving product usage data in the data warehouse. | PM Responsibility, Product Analytics Support |
| [Data Warehouse](https://about.gitlab.com/handbook/business-ops/data-team/platform/#data-warehouse) | The data warehouse step outlines where our product usage data is stored. | Product Analytics Responsibility |
| [Data Model](https://about.gitlab.com/handbook/business-ops/data-team/platform/dbt-guide/) | The data modelling step outlines how we transform raw data into a data structure that's ready for analysis. | Product Analytics Responsibility |
| [Dashboard](https://about.gitlab.com/handbook/business-ops/data-team/programs/data-for-product-managers/#creating-charts) | The dashboarding step outlines how Sisense dashboards are built. | PM Responsibility, Product Analytics Support |
| [Handbook PI Page](https://about.gitlab.com/handbook/product/performance-indicators/) | The handbook PI page describes how product performance indicators are added for each product section, stage, and group. | PM Responsibility, Product Analytics Support |
| [PI Target](#pi-target) | The target definition step outlines how targets are defined for each performance indicator. | PM Responsibility, Product Analytics Support |

### What we're aiming for

The table below outlines the exact PIs we're asking each section, stage, and group to implement by end of FY21-Q3. This table is based off the [previous guidance of focus areas](https://gitlab.com/gitlab-com/Product/-/issues/1286). The implementation status for each of these PIs will be tracked on their respective Section PI pages.

Changes to be table below require sign off from the EVP Product or VP Product Management. To make updates to the table, please submit an MR with the reason behind the change and assign it to Scott or Anoop.

Dashboard data sources:

- XMAU (SM): Dashboard can only be created from Usage Ping
- Paid XMAU (SM): Dashboard can only be created from Usage Ping
- XMAU (SaaS): Dashboard can only be created from Usage Ping, Postgres Import, Snowplow
- Paid XMAU (SaaS): Dashboard can only be created from Postgres Import

The standardized dashboard used for our PIs will have segments for both XMAU and Paid XMAU if the data source is available. Product teams may choose to focus on one or many of these segments to drive paid usage or overall usage.

| section.stage.group | XMAU (SM) | Paid XMAU (SM) | XMAU (SaaS) | Paid XMAU (SaaS) |
| ------------------- | --------- | -------------- | ----------- | ---------------- |
| dev | Yes, Section TMAU | Yes, Section TMAU | Yes, Section TMAU | Yes, Section TMAU |
| dev.create | Yes | Yes | Yes | Yes |
| dev.create.ecosystem | Yes | Yes | Yes | Yes |
| dev.create.editor | Yes | No | Yes | No |
| dev.create.gitaly | Yes | Yes | Yes | Yes |
| dev.create.knowledge | Yes | No | Yes | No |
| dev.create.source_code | Yes | Yes | Yes | Yes |
| dev.create.static_site_editor | Yes | No | Yes | No |
| dev.manage | Yes | Yes | Yes | No |
| dev.manage.access | Yes | Yes | Yes | Yes |
| dev.manage.analytics | Yes | Yes | Yes | No |
| dev.manage.compliance | Yes | Yes | Yes | Yes |
| dev.manage.import | Yes | Yes | Yes | Yes |
| dev.plan | Yes | Yes | Yes | Yes |
| dev.plan.certify | Yes | Yes | Yes | Yes |
| dev.plan.portfolio_management | Yes | Yes | Yes | Yes |
| dev.plan.project_management | Yes | Yes | Yes | Yes |
| enablement | Yes, PPI | Yes, PPI | Yes, PPI | Yes, PPI |
| enablement.enablement | Yes, PPI | Yes, PPI | Yes, PPI | Yes, PPI |
| enablement.enablement.database | Yes, PPI | Yes, PPI | Yes, PPI | Yes, PPI |
| enablement.enablement.distribution | Yes, PPI | Yes, PPI | Yes, PPI | Yes, PPI |
| enablement.enablement.geo | Yes | Yes | Yes | Yes |
| enablement.enablement.global_search | Yes | Yes | Yes | Yes |
| enablement.enablement.infrastructure | No | No | No | Yes, PPI |
| enablement.enablement.memory | Yes, PPI | Yes, PPI | Yes, PPI | Yes, PPI |
| ops | Yes, Section TMAU | Yes, Section TMAU | Yes, Section TMAU | Yes, Section TMAU |
| ops.verify | Yes | Yes | Yes | Yes |
| ops.verify.continuous_integration | Yes | No | Yes | No |
| ops.verify.pipeline_authoring | Yes | No | Yes | No |
| ops.verify.runner | Yes | No | Yes | No |
| ops.verify.testing | Yes | Yes | Yes | Yes |
| ops.package | Yes | No | Yes | No |
| ops.package.package | Yes | No | Yes | No |
| ops.release | Yes | No | Yes | No |
| ops.release.progressive_delivery | Yes | Yes | Yes | Yes |
| ops.release.release_management | Yes | Yes | Yes | Yes |
| ops.configure | Yes | No | Yes | No |
| ops.configure.configure | Yes | No | Yes | No |
| ops.monitor | Yes | Yes | Yes | Yes |
| ops.monitor.health | Yes | No | Yes | No |
| securedefend | Yes | Yes | Yes | Yes |
| securedefend.defend | Yes, GMAC | Yes, GMAC | Yes, GMAC | Yes, GMAC |
| securedefend.defend.container_security | Yes | Yes | Yes | Yes |
| securedefend.defend.insider_threat | Yes | Yes | Yes | Yes |
| securedefend.secure | Yes | Yes | Yes | Yes |
| securedefend.secure.composition_analysis | Yes | Yes | Yes | Yes |
| securedefend.secure.dynamic_analysis | Yes | Yes | Yes | Yes |
| securedefend.secure.fuzz_testing | Yes | Yes | Yes | Yes |
| securedefend.secure.static_analysis | Yes | Yes | Yes | Yes |
| securedefend.secure.threat_insights | Yes | Yes | Yes | Yes |
| securedefend.secure.vulnerability_research | Yes | Yes | Yes | Yes |

### Next Steps

1. @jeromezng @kathleentam will meet with all stages.
1. Add metrics and definitions into the Event Dictionary.
1. Instrument tracking by focusing on XMAU candidates.
1. Wait for data availability.
1. Create standardized dashboards.
1. Update handbook PI pages with XMAU definitions.
1. Add targets to each PI.

### Privacy Policy

For our current state, see: [Privacy Policy](https://about.gitlab.com/privacy/). For our roadmap, see: [Rollout Plan: Privacy Policy for Product Usage Data](https://gitlab.com/groups/gitlab-com/-/epics/907).

### Collection Framework

For our current state and roadmap, see: [Telemetry Direction Page: Collection Framework](https://about.gitlab.com/direction/telemetry/#build-collection-framework).

### Event Dictionary

We currently have 500+ existing usage ping metrics but no proper documentation of what actions they track. The Event Dictionary will allow us to create a single source of truth for the metrics and events we collect. It will also help PMs better understand what data is currently collected and what metrics can potential be used as an XMAU. The event dictionary includes:

- 500+ Usage Ping metrics
- Section, stage, or group assigned to the metric
- Metric description
- Implementation status
- Availability by plan type
- Code path of metric

Note: we've temporarily moved the [Event Dictionary](https://docs.google.com/spreadsheets/d/1VzE8R72Px_Y_LlE3Z05LxUlG_dumWe3vl-HeUo70TPw/edit#gid=618391485) from our Markdown docs into a sheet for easier editing. We'll eventually move this [back into our docs](https://docs.gitlab.com/ee/development/telemetry/event_dictionary) once the majority of changes are done.

**Instructions:**

1. Complete the [Event Dictionary for Usage Ping](https://gitlab.com/groups/gitlab-org/-/epics/4174) issue for your section: [Dev](https://gitlab.com/gitlab-org/gitlab/-/issues/234301), [Ops](https://gitlab.com/gitlab-org/gitlab/-/issues/234577), [Enablement](https://gitlab.com/gitlab-org/gitlab/-/issues/234576), [Secure & Defend](https://gitlab.com/gitlab-org/gitlab/-/issues/234578), [Growth](https://gitlab.com/gitlab-org/gitlab/-/issues/234580).
1. Open the [Event Dictionary](https://docs.google.com/spreadsheets/d/1VzE8R72Px_Y_LlE3Z05LxUlG_dumWe3vl-HeUo70TPw/edit#gid=618391485) and fill in “PM to edit” columns.
1. Plan your future events and metrics with your engineering team and add them to the Event Dictionary.

### Instrument Tracking

Work with your engineering team to instrument tracking for your XMAU. Focus on using Usage Ping as your metrics will be available on both SaaS and self-managed.

The [Usage Ping Guide](https://docs.gitlab.com/ee/development/telemetry/usage_ping.html) outlines the steps required for instrumentation. It includes:

- [What is Usage Ping?](https://docs.gitlab.com/ee/development/telemetry/usage_ping.html#what-is-usage-ping)
- [How Usage Ping works](https://docs.gitlab.com/ee/development/telemetry/usage_ping.html#how-usage-ping-works)
- [Implementing Usage Ping](https://docs.gitlab.com/ee/development/telemetry/usage_ping.html#implementing-usage-ping)
- [Developing and Testing Usage Ping](https://docs.gitlab.com/ee/development/telemetry/usage_ping.html#developing-and-testing-usage-ping)
- [Example Payload](https://docs.gitlab.com/ee/development/telemetry/usage_ping.html#example-usage-ping-payload)

Also see the [Telemetry Guide](https://docs.gitlab.com/ee/development/telemetry/) and [Snowplow Guide](https://docs.gitlab.com/ee/development/telemetry/snowplow.html).

**Instructions:**

1. Read the docs and work with your engineers on instrumentation.
1. Ask for a [Telemetry Review](https://docs.gitlab.com/ee/development/telemetry/usage_ping.html#8-ask-for-a-telemetry-review) in your MR.

### Data Warehouse

See [Data Team Platform: Data Warehouse](https://about.gitlab.com/handbook/business-ops/data-team/platform/#data-warehouse)

### Data Model

See [Data Team dbt Guide](https://about.gitlab.com/handbook/business-ops/data-team/platform/dbt-guide/)

### Data Availability

Plan instrumentation with sufficient lead time for data availability. Ensure your metrics make it into the self-managed release as early as possible.

**Timeline:**

1. Self-managed releases happen on the 22nd of each month (+30 days)
1. Wait one week for customers to upgrade to the new release and for a Usage Ping to be generated (+7 days)
1. Usage Pings are collected in the Versions application. The Versions database is imported into the Snowflake Data Warehouse manually every two weeks (+14 days). Note that we’re working to [automate this to daily imports](https://gitlab.com/gitlab-org/telemetry/-/issues/398) (bringing this down to +1 day instead).

In total, plan for up to 51 day cycle times (Examples [1](https://gitlab.com/gitlab-data/analytics/-/issues/5629#note_389671640), [2](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/31785#note_392882428)). Cycle times are slow with monthly releases and weekly pings, so, implement your metrics early.

**Instructions:**

1. Merge your metrics into the next self-managed release as early as possible.
1. Wait for your metrics to be released onto production GitLab.com. These releases currently happen on a daily basis.
1. Usage Pings are generated on GitLab.com on a weekly basis. Monitor the #g_telemetry slack channel where the Telemetry team will post the latest GitLab.com Usage Ping ([example](https://gitlab.slack.com/files/ULXG09FAL/F01905UPPL0/12-gitlab.com-usage-data-for2020-08-04.json?origin_team=T02592416&origin_channel=CL3A7GFPF)). Verify your new metrics are present in the GitLab.com Usage Ping payload.
1. Wait for the Versions database to be imported into the data warehouse.
1. Check the dbt model [version_usage_data_unpacked](https://dbt.gitlabdata.com/#!/model/model.gitlab_snowflake.version_usage_data_unpacked#columns) to ensure the database column for your metric is present.
1. Check [Sisense](http://app.periscopedata.com/app/gitlab/) to ensure data is available in the data warehouse.

### Dashboard

We need PMs to self-serve their own dashboards as data team capacity is limited. The data team will be focused on enabling self-service, advising PMs, and working on the more challenging XMAU dashboards.

To learn how to create your own dashboard, see [Data For Product Managers: Creating Charts](https://about.gitlab.com/handbook/business-ops/data-team/programs/data-for-product-managers/#creating-charts)

To update the SMAU Summary Dashboards: [GitLab.com Postgres SMAU Dashboard (SaaS)](https://app.periscopedata.com/app/gitlab/604621/GitLab.com-Postgres-SMAU) and [Usage Ping SMAU Dashboard (SaaS + Self-Managed)](https://app.periscopedata.com/app/gitlab/634200/Usage-Ping-SMAU-Dashboard), please open a [data team issue](https://gitlab.com/gitlab-data/analytics/-/issues).

**Dashboard Prioritization**

For GMAU and SMAU data issues:

- In Q3, due to very limited data team capacity, the data team capacity will be reserved primarily for GMAU and SMAU.
    - Please add `XMAU` label to the data issues.
    - Please limit the ask to XMAU only, rather than all the supporting metrics.
- If there is still need to prioritize within GMAU issues, we will work with Scott and Section Leaders on the prioritization.
    - Section Leaders are encourage to [rank XMAU Issues](https://gitlab.com/gitlab-data/analytics/-/issues/5664#note_392529098) on the [Product Section Board](https://gitlab.com/gitlab-data/analytics/-/boards/1921369?label_name%5B%5D=Product).

For non-GMAU and non-SMAU data issues:

- In Q3, the data team will have very limited capacity to support non-GMAU and non-SMAU data requests.
    - Please continue to create data issues and label Data issues with the appropriate [Product Section Label](https://gitlab.com/groups/gitlab-data/-/labels?utf8=%E2%9C%93&subscribed=&search=section).
- If there are critical or urgent data asks, please @hilaqu and your section leader in the issue, with an explanation of why. We will evaluate them on a case-by-case basis.

**Instructions:**

1. Read through [Data For Product Managers: Creating Charts](https://about.gitlab.com/handbook/business-ops/data-team/programs/data-for-product-managers/#creating-charts)
1. Self-serve your dashboard
1. If you need help, create a [data team issue](https://gitlab.com/gitlab-data/analytics/-/issues) and ask your Section Leader to prioritize.

### Handbook PI Page

There are five Product PI pages: The [Product Team page](https://about.gitlab.com/handbook/product/performance-indicators/) and section pages for [Dev](https://about.gitlab.com/handbook/product/dev-section-performance-indicators/), [Ops](https://about.gitlab.com/handbook/product/ops-section-performance-indicators/), [Secure & Defend](https://about.gitlab.com/handbook/product/secure-and-defend-section-performance-indicators/), [Enablement](https://about.gitlab.com/handbook/product/enablement-section-performance-indicators/).

We need all PMs to ensure their PIs are showing on the performance indicator pages. Based off [What we're aiming for](#what-were-aiming-for)

To do so, we need a clear way to communicate to PMs exactly which PIs are remaining. We will be [adding placeholder PIs for each section](https://gitlab.com/groups/gitlab-com/-/epics/906) into the [performance indicator file](https://gitlab.com/gitlab-com/www-gitlab-com/-/blob/master/data/performance_indicators/product.yml) so that all required PIs show in the handbook. Once a PI is implemented, the actual PI will replace the placeholder PI.For more information about how PIs and XMAUs are related to one another, see [PI Structure](https://about.gitlab.com/handbook/product/performance-indicators/#structure).

**Instructions:**

1. @jeromezng @kathleentam will [add placeholder PIs for each section](https://gitlab.com/groups/gitlab-com/-/epics/906).
1. @jeromezng @kathleentam will then meet with each Section, Stage, and Group, to understand the implementation status of each PI and document any exceptions. Exceptions for PIs will then be signed off by Scott, Anoop, and the Section Leaders.
1. Keep your [performance indicator](https://gitlab.com/gitlab-com/www-gitlab-com/-/blob/master/data/performance_indicators/product.yml) up to date as the implementation status changes.

### PI Target

As a product organization, we need to get into the habit of understanding our baselines and setting targets for each stage & group. For the PI Target step, you will work with your Section or Group Leader to define targets for each of your XMAUs.

**Instructions:**

1. Work with your Section or Group Leader to define a target.
1. Add the Target Line into your dashboard ([example](https://app.periscopedata.com/app/gitlab/527913/Product-KPIs?widget=8490496)).
1. If you need help, book a meeting with @jeromezng @kathleentam.
---
layout: markdown_page
title: "GitLab Diversity Sponsorship"
description: "GitLab strives to promote increased awareness of diversity issues. As part of efforts to meet this goal, GitLab sponsor informative and interactive events"
canonical_path: "/community/sponsorship/"
comments: false
sharing: true
suppress_header: true
---

### Diversity Sponsorship Criteria 
GitLab is striving to promote increased awareness of diversity issues. As part of GitLab's efforts to meet this goal, GitLab will sponsor informative, interactive, and reflective events.

### Requirements 
* The Program must reflect respect of differences in Race, Ethnic Background, Sexual Orientation, Gender and Ability/Disability.
* Events must be informative. The take-away must be a learning experience as it relates to diversity. 
* Attendees must be able to answer: "What did I learn about diversity?"
* The Event must be interactive and encourage audience participation. This may include lectures, discussions, demonstrations, Q&A sessions, or a combination.
* Attendees must have the opportunity to participate in discussions, ask questions, get answers, interact through demonstrations, learning exercises, roundtables, etc.
* The Event must contain a reflective element that introduces participants to a new perspective or idea. The reflective sessions can be roundtable or Q&A sessions.
* Attendees must have the ability to revisit the process and information learned, and understand practical applications for diversity in their everyday life.
* Wherever possible, attendees must be provided with a [sign-up link](https://gitlab.fra1.qualtrics.com/jfe/form/SV_51J9D8skLbWqdil?Source=d&i) to join our [First-Look](/community/gitlab-first-look/) UX research participant panel 

The Event must help to further GitLab's [values](https://about.gitlab.com/handbook/values/). When considering events to sponsor, the GitLab team values the opportunity to actively participate in the event. 
When processing applications, the team considers:
* What type of awareness will GitLab receive?
* Will attendees receive career networking? 
* Do sponsors have the ability to connect with attendees? 
* What level of engagement will GitLab have with attendees? 

In the application please include specific connections/references to GitLab's values. 

![Community Sponsorship](/images/community/gitlab-growth.jpg)

### How to apply to Diversity and Inclusion Event Sponsorship?

If you intend to apply, please leave enough lead time (at least 6 weeks, but
preferably more) for us to review your application.

Please be aware that GitLab will Sponsor up to 20 events annually. Please submit one application per organization, per calendar year.
Due to the number of applications, only approved parties will receive confirmation of sponsorship.
To suggest a Diversity, Inclusion and Belonging Event for sponsorship please use the [Diversity, Inclusion and Belonging Sponsorship Request Issue Template](https://gitlab.com/gitlab-com/diversity-and-inclusion/issues/new?issuable_template=diversity-sponsorship-request-template).


Why is fostering diversity important?

As if the moral imperative and ethical rationale was not enough,
there are also practical advantages to fostering diversity.
These events help increase the potential pool of talent to work at GitLab.
Research has also proven that more diversity is better for business in almost
every aspect. ([McKinsey, 2015](http://www.mckinsey.com/insights/organization/why_diversity_matters))

### Eligibility (Non-Diversity Events)

If you're hosting an event which doesn't meet the description of an "event which promotes diversity in technology", we'd like to thank you for considering GitLab as your sponsor. Unfortunately, we don't respond to non-diversity requests for sponsoring. We identify sponsorship opportunities and reach out ourselves.

### Sponsored Events

You can take a look at the diversity events we sponsor on our [events page](/events/).

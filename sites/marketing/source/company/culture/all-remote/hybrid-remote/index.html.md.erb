---
layout: handbook-page-toc
title: "Hybrid-Remote: understanding nuances and pitfalls"
canonical_path: "/company/culture/all-remote/hybrid-remote/"
description: Hybrid-Remote — understanding nuances and pitfalls
twitter_image: "/images/opengraph/all-remote.jpg"
twitter_image_alt: "GitLab remote team graphic"
twitter_site: "@gitlab"
twitter_creator: "@gitlab"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Introduction 

On this page, we're detailing the primary differences between all-remote and other forms of remote working.

## What is hybrid-remote?

<!-- blank line -->
<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/_HhlqwJsNyM" frameborder="0" allowfullscreen="true"> </iframe>
</figure>
<!-- blank line -->

*In the [GitLab Unfiltered](https://www.youtube.com/channel/UCMtZ0sc1HHNtGGWZFDRTh5A) video above, Darren (GitLab) and Anna-Karin (Because Mondays) discuss a number of challenges and solutions related to remote work, transitioning a company to remote, working asynchronously, and defaulting to documentation.*

Hybrid-remote (which can be referred to as part-remote), is [different than all-remote](/blog/2018/10/18/the-case-for-all-remote-companies/). In an all-remote company, there is no single headquarters, and each team member is free to live and work in any place they choose. Everyone, including executives, is remote, as there are no offices to come to. 

Hybrid-remote is currently more common than all-remote, as it is easier for large, established companies to implement. In a hybrid-remote scenario, there is one or more offices where a subset of the company commutes to each day — working physically in the same space — paired with a subset of the company that works remotely. 

These institutions are primarily colocated, but allow remote work. 

<!-- blank line -->
<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/QFbrPR0jAYs?start=3" frameborder="0" allowfullscreen="true"> </iframe>
</figure>
<!-- blank line -->

*In the [GitLab Unfiltered](https://www.youtube.com/channel/UCMtZ0sc1HHNtGGWZFDRTh5A) video above, GitLab co-founder and CEO Sid Sijbrandij chats with Maren Kate, founder at AVRA Talent Partners.* 

In discussing the decision to go all-remote at GitLab, Sid shares the following.

> For us, it was really important that people didn't have to come to the office to get information necessary for career opportunities. 
>
> From very early on, we started writing things down. During Y Combinator, they told us "Look, remote work is for engineering, but not finance, marketing, or sales."
>
> So, we got an office. People got hired, they came there, **but after a few days they stopped showing up**. 
>
> [Coming to the office] wasn't needed. They weren't getting any extra information. They were on Slack, on Zoom, in Google Docs, in GitLab pages, in GitLab Issues, in GitLab merge requests — they didn't need to be there. 
>
> **It's not that people like their commute; it's just that people don't want to miss out.** If you make sure that people don't miss out, you can be remote, too. It takes a lot of effort and focus to make sure all conversations are captured appropriately and that everything is documented. - *GitLab co-founder and CEO Sid Sijbrandij*

## Blueprints and examples of companies transitioning to hybrid-remote

COVID-19 has created a wave of companies intentionally shifting to remote-first. For some firms, unwinding all of their office space and [becoming an all-remote organazation](/company/culture/all-remote/transition/) is not practical. Thus, many are emerging as hybrid-remote companies. We believe it is useful for transitioning companies to see how others are embracing and making this shift. Below are examples of companies who are publicly, transparently sharing their journey.

1. HubSpot — *[The Future of Work at HubSpot: How We're Building a Hybrid Company](https://www.hubspot.com/careers-blog/future-of-work-hybrid)*
1. Figma — *[How work is changing at Figma](https://www.figma.com/blog/how-work-is-changing-at-figma/)*
1. Coinbase — *[Post COVID-19, Coinbase will be a remote-first company](https://blog.coinbase.com/post-covid-19-coinbase-will-be-a-remote-first-company-cdac6e621df7)*
1. Shopify — *[Digital by default](https://www.shopify.com/careers/work-anywhere)*
1. Twitter — *[Moving to mandatory work from home globally + supporting our employees](https://blog.twitter.com/en_us/topics/company/2020/keeping-our-employees-and-partners-safe-during-coronavirus.html)* 
1. Slack — *[A new guide for adapting to a radically different workplace](https://slack.com/blog/transformation/navigating-the-disruption-of-work)*
1. Sike Insights — *[EQ & Remote Managers Report](https://bit.ly/3iX519B)*

## Do hybrid-remote employees have a commute?

![GitLab transport illustration](/images/all-remote/gitlab-transport.jpg){: .medium.center}

Some hybrid-remote arrangements do involve *regular* commutes to the office, though not *daily* commutes. For example, a remote employee in a hybrid-remote organization may travel to an office one week each month for regularly scheduled in-person interactions, while working from a location of their choosing the rest of the month.

While this scenario may still be preferred over one where remote employees are *not* invited to visit in-person offices, it isn't quite as flexible as all-remote. There's still a commute involved, which can take the majority of a day in both directions for commutes involving flights. 

## Are there advantages to hybrid-remote?

Hybrid-remote arrangements such as the above offer unique advantages. For hybrid-remote employees who can count on a regular trip to a destination funded by their employer, they're able to [plan micro-trips around their business travel](https://thepointsguy.com/guide/best-jobs-for-traveling/). 

For hybrid-remote employees with a taste for exploration and a flexibile schedule, these built-in business trips can serve as [jumping-off points](https://thepointsguy.com/guide/maximize-business-travel/) for exploring new locales that they may not have the means to explore in an all-remote company.

For employers who are committed to a colocated model, but wish to expand their recruiting pipeline beyond the city where they are headquartered, allowing remote employees to join their ranks can be beneficial. Employers may be able to find exceptional team members in a more [diverse array of locales](/company/culture/all-remote/hiring/index.html), pay them [local rates](/blog/2019/02/28/why-we-pay-local-rates/), and sidestep ongoing talent wars in major metropolitan areas. 

In doing so, employers would effectively enact a hybrid-remote model, which requires additional considerations to prevent remote employees from being significantly disadvantaged — a point we'll cover in detail below.

## Disadvantages to hybrid-remote

<!-- blank line -->
<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/IFBj9KQSQXA" frameborder="0" allowfullscreen="true"> </iframe>
</figure>
<!-- blank line -->

*In the [GitLab Unfiltered](https://www.youtube.com/channel/UCMtZ0sc1HHNtGGWZFDRTh5A) video above, GitLab co-founder and CEO Sid Sijbrandij chats with InVision Chief People Officer Mark Frein on the future of all-remote. In the [conversation](/blog/2019/07/31/pyb-all-remote-mark-frein/), the two discuss the differences between all-remote and hybrid-remote.*

All things being equal, employees longing for additional freedom, autonomy, and workplace flexibility will likely view a hybrid-remote arrangement as superior to a colocated arrangement — one which *requires* a commute and an in-person presence on a daily basis. 

Said another way, "some remote" is often viewed as superior to "no remote." Though far from ideal, it can be rationalized that fewer career opportunities, added judgement, and difficulties in bonding are prices worth paying to live and work where one wants.

There are considerations when accepting a role in a hybrid-remote company, and it's important to be mindful of these potential downsides.

1. **Hybrid-remote employees may have less access to information**. Unless you work for an employer that [documents everything](/handbook/values/#write-things-down), you may be asked to handle your day-to-day duties with less information — and incomplete information — compared to in-person colleagues. Over time, this can lead to more mistakes, confusion, frustration, and even [underperformance](/handbook/underperformance/). 
1. **Fewer career and development opportunities**. Hybrid-remote employees who are out of sight may be [passed over](https://hbr.org/2017/11/a-study-of-1100-employees-found-that-remote-workers-feel-shunned-and-left-out) for promotions, advancement, and development [opportunities](https://news.ycombinator.com/item?id=15994294). They may also have fewer opportunities to more horizontally within the organization, and less influence to create a new role to serve evolving business needs.
1. **The feeling of being a satellite office**. Hybrid-remote employees must put effort towards not being treated as less-than within the organization. It is important to surface relevant inquiries during the interview process as to how remote colleagues are [onboarded](/company/culture/all-remote/learning-and-development/), included, and perceived by others. Some employees may not be fazed by this treatment, but it can take a mental and emotional toll on others.  
1. **Managing guilt**. It is not uncommon to hear remote workers [express guilt](https://www.glassdoor.com/blog/work-from-home-guilt/) if they work in a company which is primarily colocated. Their socializing involves colleagues who may complain about commutes, or express sadness due to an inability to attend a certain family function. There are inherit inequalities in this arrangement, requiring the remote employee to emphathize with in-person colleagues despite not being required to endure the same commutes and inflexibility. 
1. **The burden of lobbying for remote**. If an employee is brought on in a remote capacity, but this arrangement is not supported equally across teams and managers, a situation may arise where the remote employee is constantly justifying the perceived privilege of not being forced to commute into a physical office. 
1. **Determining whether remote is truly offered and supported**. Many large companies will tolerate remote employees, but they will not openly advertise roles as remote, nor will they publicly admit that they support remote work. This creates an exhausting game of hide-and-seek when searching for roles, in addition to searching for remote-friendly managers and teams *within* such an organization.
1. **Risk of being made an example of**. It is possible for remote employees in a primarily colocated company to be asked questions like "So, how did you finagle a remote arrangement?" This places remote employees in a difficult situation. Either they choose to champion the cause of empowering even more colleagues to work remotely, potentially harming their reputation in the process, or they appear unhelpful by keeping the perceived perk to themselves. 
1. **Demands for overperformance**. When you're a remote employee working with colleagues who endure long commutes each day, you may encounter pressure — however subtle — to deliver results beyond those expected of in-person team members. This stems from a toxic culture of envy, where colocated employees deduce that if they must endure inflexibility and commutes, remote colleagues must produce additional results as to not get off easier. 

## All-Remote Upgrade

![GitLab values illustration](/images/all-remote/gitlab-values-tanukis.jpg){: .medium.center}

The [movement](/company/culture/all-remote/stages/) from hybrid-remote to all-remote is defined at GitLab as an `All-Remote Upgrade`. For reference, in January 2020, 17% of new hires attending the [CEO 101 call](/company/culture/gitlab-101/) moved from a hybrid-remote model to all-remote at GitLab. 

This highlights another reality: [not all remote models are equal](/blog/2019/09/04/not-all-remote-is-created-equal/), nor do they create [equality](/blog/2019/12/06/how-all-remote-supports-inclusion-and-bolsters-communities/). All-remote is the [purest](/blog/2018/10/18/the-case-for-all-remote-companies/) form of remote work, where every individual is treated as a [first-class team member](/company/culture/all-remote/people/). 

Through GitLab's [All-Remote Stories](/company/culture/all-remote/stories/) and the [Remote Work GitLab Unfiltered playlist](https://www.youtube.com/playlist?list=PL05JrBw4t0Kq7QUX-Ux5fOunQotqJbECc), you can learn more from GitLab team members on what all-remote has meant to them and how it has shaped their lives and communities. 

## Differences between all-remote and remote-first

In a remote-first organization, the *default* is remote. While there may be a company headquarters, or even an array of satellite offices, everything from how meetings are handled to onboarding new hires is [structured through a remote lens](https://stackoverflow.blog/2017/02/08/means-remote-first-company/).

Said another way, there is a culture of assuming that remote is the norm, not the exception, and processes are established to reinforce that.

If you're considering working for a remote-first company, consider asking the following.

1. Do you have a headquarters?
1. Does the executive team work remotely, or do they all congregate in the same physical space day-to-day?
1. Where and how are all-hands calls handled?
1. Are interviews and onboarding handled on-premise or via video call?
1. Do team members have [hybrid calls](/handbook/communication/#hybrid-calls-are-horrible), or does each person have their own equipment?
1. What procedures are in place to ensure that remote employees are granted [learning and development](/company/culture/all-remote/learning-and-development/) and promotion opportunities? 

It is possible to find remote-first companies with a healthy, understanding culture that works to support both colocated and remote colleagues. Prospective employees should do their own due diligence to make sure the requisite values are established and lived out. 

## GitLab Knowledge Assessment: Understanding a Hybrid-Remote Environment

Anyone can test their knowledge on Understanding a Hybrid-Remote Environment by completing the [knowledge assessment](https://docs.google.com/forms/d/e/1FAIpQLSc-aKsS7Yu1-wwlpHsfEtHt8CkKK5n5UhnnU6ixTlM00Dz9cw/viewform). Earn at least an 80% or higher on the assessment to receive a passing score. Once the quiz has been passed, you will receive an email acknowledging the completion from GitLab. We are in the process of designing a GitLab Remote Certification and completion of the assessment will be one requirement in obtaining the [certification](/handbook/people-group/learning-and-development/certifications). If you have questions, please reach out to our [Learning & Development](/handbook/people-group/learning-and-development) team at `learning@gitlab.com`.

## <%= partial("company/culture/all-remote/is_this_advice_any_good_remote.erb") %>

## Contribute your lessons

GitLab believes that all-remote is the [future of work](/company/culture/all-remote/vision/), and remote companies have a shared responsibility to show the way for other organizations who are embracing it. If you or your company has an experience that would benefit the greater world, consider creating a [merge request](https://docs.gitlab.com/ee/user/project/merge_requests/) and adding a contribution to this page.

----

Return to the main [all-remote page](/company/culture/all-remote/).

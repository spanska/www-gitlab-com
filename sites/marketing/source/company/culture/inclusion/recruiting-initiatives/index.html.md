---
layout: markdown_page
title: "Diversity, Inclusion & Belonging Recruiting Initiatives"
description: "Learn more about GitLab Diversity, Inclusion & Belonging Recruiting Initiatives."
canonical_path: "/company/culture/inclusion/recruiting-initiatives/"
---

At GitLab, Diversity, Inclusion & Belonging is infused into our company culture, from our [values](/handbook/values/) to our [all-remote way of working](/company/culture/all-remote/). 
The GitLab Recruiting team partners closely with our [Diversity, Inclusion & Belonging](/company/culture/inclusion/) team to ensure we’re building a diverse and inclusive workforce around the globe as the company continues to grow. 


## Talent Brand Initiatives

### Spotlighting GitLab team members 

Our goal is to tell the story of what it's like to work at GitLab through our people. We'll do this through blogs, videos, social media, and other platforms. 
By sharing these stories, we're able to highlight the diversity of our team in an authentic way.

* Blog posts from team members
* Recorded videos and panels 
   - [What Diversity, Inclusion and Belonging means to the Sales team](https://www.youtube.com/watch?v=paPXSdfl_To&feature=youtu.be) 
   - [Get to know the Support team at GitLab](https://youtu.be/fLTs1oiKabI) 
   - [Women at GitLab fireside chat](https://youtu.be/qS0kebPUhTo) from Virtual Contribute 2020 
   - [Tips for working remotely + parenting](https://youtu.be/TYdPXSYpBcg)

### Employer and Diversity, Inclusion & Belonging awards

One of the ways we raise awareness about life at GitLab is by applying for employer awards. These award programs are typically run by third-party organizations or media outlets. 
A number of the awards we've applied for (or will apply for in the future) are specifically focused on recognizing companies that are diverse and inclusive employers. 

* [Awards we’ve won](/handbook/people-group/employment-branding/#employer-awards-and-recognition) 
* [Awards we’re applying for on behalf of GitLab](https://gitlab.com/groups/gitlab-com/marketing/-/epics/565) 

### Initiatives we’re exploring

* Strategic partnerships with gender diversity platforms
* Recorded podcasts
* Virtual events: Culture Open House 
* Updating the look and feel across digital channels to ensure the diversity of our team is reflected

## Sourcing Initiatives 

### Sourcing for Underrepresented Groups 

With our [outbound recruiting model](/jobs/faq/#gitlabs-outbound-recruiting-model), the Recruiting team is able to focus on sourcing the most talented candidates for GitLab’s open roles. This will allow us to continue to build a diverse team as we grow.

#### Diversity sourcing training

The Recruiting team is providing additional training on Diversity Sourcing and Recruiting best practices to all GitLab team members.

#### SURG initiative

The Recruiting team collaborates on sourcing underrepresented candidates for four of our highest priority roles on a monthly cadence as part of the Sourcing Underrepresented Groups (SURG) initiative. This initiative fosters and opportunity for the entire recruiting team to collaborate together and source candidates for roles they may not typically recruit for. The SURG initiative is seen as an additional effort alongside our usual sourcing activities. You can follow our progress in the #SURG slack channel.

## Hiring Initiatives 

### Inclusive interviewing 
We are building an inclusive workforce to support every demographic. One major component is ensuring our hiring team is fully equipped with the skills necessary to connect with candidates from every background. We strive to have a hiring team that is well-versed in every aspect of diversity, inclusion and cultural competence. We are helping the unconscious become conscious. Our number one priority is a comfortable and positive candidate experience. Our interviewing guide describes [how you can request an adjustment to your interview process](https://about.gitlab.com/handbook/hiring/interviewing/#adjustments-to-our-interview-process). 

* To aid in our inclusive hiring practices, we've implemented [Greenhouse Inclusion](/handbook/hiring/greenhouse/#greenhouse-inclusion).

### Inclusive language in Recruitment

We want all prospective candidates, including those from underrepresented groups to have a sense of belonging at GitLab. Inclusive language helps build a sense of belonging and does not alienate underrepresented groups. Written communication that does not use biased language, but is instead neutral, will help increase response rates and levels of interest in GitLab from people in underrepresented groups.

* The Recruiting team is reviewing and making sure all required edits are done to GitLab’s recruiting templates.

### Speaking with TMRG members in the hiring process

During our hiring process, there's an **optional** step where candidates can request to meet with a team member from one of our [Team Member Resource Groups (TMRGs)](/company/culture/inclusion/erg-guide/#how-to-join-current-tmrgs-and-their-slack-channels). Candidates can request this at any time throughout the process, and we will also proactively offer this to a candidate when they reach the reference check stage. Whether or not the candidate decides to take us up on this offer will have no impact on our overall hiring decision.

When a candidate requests to meet with an TMRG team member, their Recruiter or Candidate Experience Specialist will share a message in the respective TMRG Slack channel. To aide with scheduling, the message will include the candidate’s time zone and a request for volunteers who would be willing to speak to that person for a 25-minute Zoom call. Once a volunteer has been found the Recruiter or Candidate Experience Specialist will share the TMRG members’ Calendly link, GitLab team page profile, and request the candidate book in a 25-minute call with the GitLab team member. 

If a volunteer has not been found within 24 hours the CES team member will reach out to the [TMRG lead(s)](/company/culture/inclusion/erg-guide/) and request assistance with scheduling. If a volunteer has not been found within 2 business days of the request, the CES will ask the TMRG lead to take part in the conversation. 

As a GitLab team member taking part in these calls, you'll need to have [completed interviewer training](/handbook/hiring/interviewing/). On the call, we advise you to start with a short introduction to you and your role here at GitLab. From here, we advise you to let the candidate lead the conversation as the goal is for you to answer their questions and offer insight into how we work. 

These calls don’t require you to submit a scorecard in Greenhouse. If a candidate mentions something that you see as a red flag (e.g. the candidate states they understand our values and demonstrate an unwillingness to adhere to them) or shares something that would help us set them up for success, we advise you to share the details of this with the hiring manager for the role they’re interviewing for. It will be the responsibility of the hiring manager to review this and decide whether we need to alter the hiring, offer, or onboarding process for the candidate. 

### Lack of dates on your resume 
If you remove dates or don't have dates on your resume or public job profile, GitLab will not assume that you did not complete that task. We understand that some people are not comfortable sharing dates or date ranges, and that's ok with us here at GitLab.  

## Return to the Main Diversity, Inclusion & Belonging Page

For a full overview of GitLab's diversity, inclusion and belonging initatives, please [return to the main page.](/company/culture/inclusion/)

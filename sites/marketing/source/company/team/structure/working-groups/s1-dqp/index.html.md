---
layout: markdown_page
title: "S-1 Filing Data Quality Process"
description: "The S-1 Filing Data Quality Process to develop a data quality process around S-1 metrics. Find more information here!"
canonical_path: "/company/team/structure/working-groups/s1-dqp/"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Attributes

| Property     | Value |
|--------------|-------|
| Date Created | March 6th, 2020 |
| Estimated Date Ended   | May 1st, 2020 |
| Slack        | [#wg_s1_dqp](hhttps://join.slack.com/share/IUP0ZEK51/REht43Huprp2J5KPGwZ3Fsif/enQtOTc1MDMzNDk1MTcxLTlhYTBmOTY3NDJhMjVlOTZlNzEzNjZjNjBjZDA1NzQ0YTFhMzEyMzA3Yzc2ZWIxMmQzM2JmNTg3YjQ2YjAyYWY) (only accessible from within the company) |
| Google Doc   | [S-1 Filing Data Quality Process Working Group Agenda](https://docs.google.com/document/d/1tWsQiQBiECP0MhG8wK4U1TMc52UXUvjPrbqazwuvEa0/edit#) (only accessible from within the company) |


## Business Goal

- Develop a data quality process around S-1 metrics


## Exit Criteria

- [S-1 Epic](https://gitlab.com/groups/gitlab-com/-/epics/312)
- [DQP Technical Epic](https://gitlab.com/groups/gitlab-data/-/epics/86)


## Roles and Responsibilities

| Working Group Role    | Person                | 
|-----------------------|-----------------------|
| Facilitator           | Chase Wright          | 
| Functional Lead(s)    | Craig Mestel, Bryan Wise, Dale Brown|
| Member                | Israel Weeks, Kat Tam, Derek Atwood, Justin Stark, Magda Sendal | 
| Executive Stakeholder | Paul Machle |

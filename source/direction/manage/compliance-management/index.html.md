---
layout: markdown_page
title: "Category Direction - Compliance Management"
description: "The goal of Compliance Management is to change the current paradigm for compliance to create an experience that's simple and friendly."
canonical_path: "/direction/manage/compliance-management/"
---

- TOC
{:toc}

## Compliance Management

| | | |
| --- | --- | --- |
| **Stage** | **Maturity** | **Content Last Reviewed** |
| [Manage](/direction/dev/#manage) | [Viable](/direction/maturity/) | `2020-07-29` |

## Introduction and how you can help

Thanks for visiting this direction page on Compliance Management in GitLab. If you'd like to provide feedback or contribute to this vision, please feel free to open a merge request for this page or comment in the [corresponding epic](https://gitlab.com/groups/gitlab-org/-/epics/720) for this category.

Compliance is a concept that has historically been complex and unfriendly. It evokes feelings of stress, tedium, and a desire to avoid the work entirely. There is often a disconnect between your company's policies and the features and settings that exist within a service provider like GitLab. Further compounding this challenge is a lack of visibility provided about the state of your account groups, projects, teams, etc within an external service or application. Compliance Management aims to bring compliance-specific data and features that focus on raising awareness and visibility of the compliance state of your GitLab groups and projects to help you make data-informed decisions about your organization's use of GitLab.

### Overview

The goal of **Compliance Management** is to change the current paradigm for compliance to create an experience that's simple and friendly. Compliance with GitLab should just happen in the background. Managing your compliance program should be easy and give you a sense of pride in your organization, not a stomach ache.

This category focuses on building features and experiences to enable compliance professionals to easily view compliance data for groups and projects; focus their time only on groups or projects that require attention; and define their organization's policies through settings and configuration.

##### Use Cases

* [Cameron (Compliance Manager)](https://about.gitlab.com/handbook/marketing/product-marketing/roles-personas/#cameron-compliance-manager) needs a single place to view key compliance data (e.g. segregation of duties, pipelines and project security grades) about their GitLab groups and projects to ensure their teams are operating appropriately, so the organization can maintain a good compliance posture with their use of GitLab.
* [Cameron (Compliance Manager)](https://about.gitlab.com/handbook/marketing/product-marketing/roles-personas/#cameron-compliance-manager) needs to implement [separation of duties controls](https://about.gitlab.com/direction/manage/#separation-of-duties) within GitLab to prevent the risk of a single individual having the ability to carry out multiple job functions, so the organization can mitigate risk and meet the compliance controls of a multitude of compliance frameworks.
* [Cameron (Compliance Manager)](https://about.gitlab.com/handbook/marketing/product-marketing/roles-personas/#cameron-compliance-manager) needs a way to identify when a project is subject to compliance requirements to ensure appropriate policies and procedures are in place, so they can be managed easily and reported on with minimal effort.

#### Target Audience

* [Cameron (Compliance Manager)](https://about.gitlab.com/handbook/marketing/product-marketing/roles-personas/#cameron-compliance-manager)
* [Sam (Security Analyst)](https://about.gitlab.com/handbook/marketing/product-marketing/roles-personas/#sam-security-analyst)

#### Challenges to address

Separation of duties is a core concept among all compliance frameworks and is generally a risk management practice for organizations. Within GitLab, there are many areas where [separation of duties](https://about.gitlab.com/direction/manage/#separation-of-duties) is required, such as for merge requests, within CI/CD pipelines, or even changing certain project settings. This is a broad area to cover, but it's also one of the most important requirements our customers have. The implementation of separation of duties features will require collaboration across multiple GitLab product groups to ensure we've covered all of our customers' needs.

* What success looks like: GitLab should have features and experiences in place that allow compliance-minded organizations to configure and enforce separation of duties within GitLab. Customers should be able to require more than a single individual be involved with certain actions, such as merge requests or within CI/CD pipelines, to manage risk for their organization and meet their compliance requirements. This can manifest as [two-person approvals](https://gitlab.com/gitlab-org/gitlab/-/issues/219386), a specific [deployer role](https://gitlab.com/gitlab-org/gitlab/-/issues/201898), and bringing merge request approvals to the [group level](https://gitlab.com/gitlab-org/gitlab/-/issues/1111) to prevent maintainers from modifying these settings.

Enterprises operating in regulated environments need to ensure the technology they use complies with their internal company policies and procedures, which are largely based on the requirements of a particular legal or regulatory framework (e.g. GDPR, SOC 2, ISO, PCI-DSS, SOX, COBIT, HIPAA, FISMA, NIST, FedRAMP) governing their industry. Customers need features that enable them to comply with these frameworks beginning with defining the rules, or controls, for their GitLab environment.

* What success looks like: Using [compliance framework project labels](https://docs.gitlab.com/ee/user/project/settings/#compliance-framework), administrators and group owners should be able to clearly label their GitLab projects. This association should help all users in a GitLab namespace know when a project has special requirements for compliance. These labels should evolve to be tied to certain compliance controls that govern how the groups and projects operate without requiring additional configuration.

When customers adhere to internal or external compliance frameworks, often times a need for customization arises. One organization's process for an activity can vary greatly from another's and this can create friction or usability issues. To facilitate better usability within GitLab towards compliance efforts, we'll be introducing features that enable customers to define specific policies or requirements their users and instance should abide by.

* What success looks like: Customers will be able to specify specific rules their users must follow at an instance, group, project, and user-level to maintain tighter control of their environments. These rules should be configurable at the appropriate level and provide a "standard" settings based on the specific compliance framework(s) an organization adheres to. These rules should be selectively enforced for regulated projects only and should prevent changes by unauthorized users.

In almost all cases, compliance controls for an organization focus on reducing overall risk, enforcing separation of duties, and implementing remediation processes. Without features to support these efforts, administrators for these organizations are faced with few workarounds: they can tolerate higher risk, lock down an instance and manage additional administrative overhead at the price of velocity, or build and maintain layers of automation on their own. For enterprises to thrive, this is a problem that demands a high-quality, native solution in GitLab.

* What success looks like: Customers should be able to transfer internal company processes to GitLab's environment to meet their needs, but without the complexity and headache of massive configuration time. This could manifest as a way of defining rules or policies within GitLab that can serve as a reference point for determining compliant versus non-compliant actions.

Compliance-minded organizations are data-informed and need visibility into all of their business operations to make the best decisions. Currently, GitLab does not aggregate the specific information organizations need to make these compliance decisions or monitor compliance status for their groups and projects. The information exists in many cases, but is simply not consolidated and presented in a way that makes compliance a simple, friendly task. Organizations have to dig for information in many disparate areas of the GitLab application and then need to build custom API-driven solutions to extract the data they need for internal compliance management or for reporting to auditors.

* What success looks like: GitLab provides a native, in-app experience for data and insights centered around compliance for organizations. This should manifest as a dashboard to track the progress and status of groups, projects, MRs, commits, etc. in the context of a specific compliance framework or program. These insights should be derived by measuring activities in GitLab against specific compliance controls, such as separation of duties, access controls, SDLC policy, and more.

### Where we are Headed

The [Compliance Dashboard](https://gitlab.com/groups/gitlab-org/-/epics/2537) continues to evolve to meet the needs of our customers managing their compliance programs. Our goal is to ensure this dashboard can answer as many questions as possible, saving your the time and effort of digging through individual projects. We want to surface all of the key compliance signals (e.g. segregation of duties, pipelines and project security grades) for you throughout a group so you can immediately, and more efficiently, hone in on the areas that actually require attention.

This dashboard currently focuses on the most recent merged MR and we'll be extending it to cover issues and projects (at a high level) as well. As we focus on compliance management, we want to build features and experiences that enable you to monitor your GitLab compliance posture much easier, saving you time and headache from doing things the traditional way.

Extending the dashboard to include relevant audit or compliance issues means be adding more enterprise templates like the [HIPAA Audit Protocol project template](https://docs.gitlab.com/ee/gitlab-basics/create-project.html#enterprise-templates), starting with SOX and SOC2. These templates, pre-loaded with issues that map to specific requirements within these respective compliance frameworks, allow you to manage portions of your audit within GitLab itself. This is important because GitLab will eventually **automate evidence collection**, which you can then export from GitLab and import into your existing GRC tool(s). This also provides a common area for you to collaborate with your developers and internal compliance team.

Adding coverage for projects to the compliance dashboard means leveraging [project compliance framework labels](https://docs.gitlab.com/ee/user/project/settings/#compliance-framework) to designate the regulated projects we should track. Currently, the compliance dashboard reports on all recently merged MRs from every project, including unregulated projects. Iterating towards our vision to reduce the complexity and time required of compliance management in GitLab means reducing the total scope of projects you need to monitor in the first place. For those organizations that need only monitor specific, regulated projects, we can surface specific insights about these projects based on your feedback.

The [Compliance Dashboard](https://gitlab.com/groups/gitlab-org/-/epics/2537) should be your "one stop shop" for everything you need to know to focus your efforts and save time. The more questions we can answer within this dashboard, the less time you're spending digging through logs, projects, settings, and other areas. Compliance should be simple and friendly and that starts with a single, easy-to-use place to start your journey.

## What's Next & Why

Compliance Management will initially be focused on the SOC2, SOX, and HIPAA compliance frameworks because these three frameworks appear to be some of the most common among GitLab customers. Additionally, the features we build for these frameworks will inherently add value to other organizations who are managing compliance with other frameworks due to the fundamental nature of many requirements the various frameworks share. For example, adding access control features to support HIPAA could potentially satisfy requirements for PCI-DSS.

We'll be introducing better control at the [project level](https://gitlab.com/groups/gitlab-org/-/epics/2087) with features like [controls definition](https://gitlab.com/groups/gitlab-org/-/epics/2491) and [compliance checks in merge requests](https://gitlab.com/gitlab-org/gitlab/issues/34830). We'll also focus on [group-level](https://gitlab.com/groups/gitlab-org/-/epics/2187) controls to continue adding necessary flexibility for you to manage compliance for groups.

We will continue to iterate on the [Compliance Dashboard](https://gitlab.com/groups/gitlab-org/-/epics/2537) to bring necessary compliance context into a single view for easy analysis and action.

| Compliance Dashboard (Projects) | Compliance Dashboard (Merge Requests) |
| ------ | ------ |
| ![](https://about.gitlab.com/images/direction/manage/compliance_dashboard-01.png) | ![](https://about.gitlab.com/images/direction/manage/compliance_dashboard-02.png) |

## Maturity

Compliance Management is currently in the **minimal** state. This is because we now have an MVC of the [Compliance Dashboard](https://gitlab.com/groups/gitlab-org/-/epics/2537), you can [designate regulated vs. unregulated projects](https://docs.gitlab.com/ee/user/project/settings/#compliance-framework) and have some existing settings that serve as compliance controls, but these features are not yet used by significant numbers of users to solve real problems.

* You can read more about GitLab's [maturity framework here](https://about.gitlab.com/direction/maturity/), including an approximate timeline.

In order to bring Compliance Management to the **viable** state, we will be implementing features that provide GitLab group owners and administrators with more, and better, compliance controls for their GitLab environments. These controls should make it easier to manage compliance within GitLab, such as maintaining separation of duties, establishing clear change control workflows, and preventing changes to these controls by unauthorized users except for emergency or exception situations.

* You can follow the **viable** maturity plan in [this epic](https://gitlab.com/groups/gitlab-org/-/epics/2423).

Once we've achieved a **viable** version of Compliance Management, achieving a **complete** level of maturity will involve collecting customer feedback and evolving the [Compliance Dashboard](https://gitlab.com/groups/gitlab-org/-/epics/2537) further and ensuring a more complete coverage of group-level and instance-level compliance controls that enable you to confidently and effectively manage the compliance posture of your GitLab environment. Assuming we're on the right track, we'll continue to focus on these areas:

* Increase the number of settings and features you can affect by assigning a framework to a group or project.
* Increase the comprehensiveness of "out-of-the-box" Compliace Management to incorporate evidence collection, reporting, and automation of auditing tasks.
* Improve the visibility and consolidation of compliance-relevant data points in the Compliance Dashboard.
* Simplify repeatable workflows, such as evidence collection, audit project management, and project creation in a regulated context.

Finally, once we've achieved a ruleset that's sufficiently flexible and powerful for enterprises, it's not enough to be able to define these rules - we should be able to measure and confirm that they're being adhered to. Achieving **Lovable** maturity likely means further expansion of the two dimensions above, plus visualizing/reporting on the state of Compliance Management across the instance.

### User Success Metrics

We'll know we're on the right track with **Compliance Management** based on the following metrics:

* An increase in the amount of time spent viewing the [Compliance Dashboard](https://gitlab.com/groups/gitlab-org/-/epics/2537)
* An increase in the number of projects using [compliance framework labels](https://docs.gitlab.com/ee/user/project/settings/#compliance-framework)
* An increase in the number of [enterprise templates](https://docs.gitlab.com/ee/gitlab-basics/create-project.html#enterprise-templates) being created

### Competitive landscape

To be completed.

### Analyst landscape

The feedback we've received about this category has been supportive of our direction to save compliance professionals time and make compliance tasks easier. While there are companies and applications that exist as holistic GRC solutions, GitLab is well-positioned to make a lot of the compliance process easier and automated by enabling customers to leverage the data they're already generating in their usage of GitLab.

### Top Customer Success/Sales issue(s)

### Top user issue(s)

* [Immediately remove a soft-deleted project](https://gitlab.com/gitlab-org/gitlab/-/issues/191367)
* [Configure Protected Branches at the Group and Instance Level](https://gitlab.com/gitlab-org/gitlab/-/issues/18488)
* [Support default approvers setting at group level](https://gitlab.com/gitlab-org/gitlab/-/issues/1111)

### Top Vision issue(s)

* [Compliance Dashboard](https://gitlab.com/groups/gitlab-org/-/epics/2537)

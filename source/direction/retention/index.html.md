---
layout: markdown_page
title: Product Direction - Retention
description: The Retention Team at GitLab focus on uncovering and addressing the leading reasons for subscription cancellation through testing and iteration. 
canonical_path: "/direction/retention/"
---

### Overview

The Retention Team at GitLab focuses on uncovering and addressing the leading reasons for subscription cancellation through hypothesis based testing and iteration. We will analyze churn from both a revenue and net customer perspective and will use that information to inform prioritization. 

We will gain a deep understanding of the top reasons for churn by leveraging qualitative data from our customers and by partnering with Customer Success, Sales, and Customer Support. 

In addition to the qualitative data we will analyze quantitative data to understand the key signals and early indicators that an account is at-risk or successful. This will require taking many different views of our data and understanding where there is correlation between a given activity and overall retention within a give stage as well as with annual renewals. Part of our effort will involve defining a retention metric that is not revenue based and it will likely be a combination of activity within a given time period. 

Once we have data in place to inform our hypotheses, our testing will likely focus on identifying at-risk customers and guiding them back to successful. 

Our ultimate goal is to keep teams happy and engaged with GitLab so they can continue to realze and benefit from the value that GitLab provides. 

**Retention Team**

Product Manager: [Michael Karampalas](/company/team/#mkarampalas) | 
Engineering Manager: [Phil Calder](/company/team/#pcalder) | 
UX Manager: [Jacki Bauer](/company/team/#jackib) | 
Product Designer: [Tim Noah](/company/team/#timnoah) | 
Full Stack Engineer: [Jeremy Jackson](/company/team/#jejacks0n) | 
Full Stack Engineer: [Jay Swain](/company/team/#jayswain)

**Retention Team Mission**

Encourage customers to keep using GitLab by: 
1. Enabling customers early in their lifecycle to become activated and succesful by experiencing the full value of GitLab as complete DevOps platform
1. Providing a simple, easy, and transparent renewal and true-up process that can be both flexible for our larger customers and automated for our smaller ones
1. Optimizing and improving the experience of when a renewal is coming up through email and in-app messaging  
1. Identifying and coaching at-risk users by partnering with our Customer Success team 
1. Helping our users realize and experience the full value of the GitLab offering 


**Retention KPI**

Subscription retention (SaaS + Self-managed)
* The % of subscriptions or licenses that are renewed for a given cohort. 
Goal: 
* Self-managed: 75%  
* Gitlab.com: 65% 

_Supporting performance indicators:_
% of accounts with auto-renew enabled 


### Problems to solve

Do you have challenges? We’d love to hear about them, how can we help GitLab contributors stay happy and engaged with GitLab? 

* The renewal process is confusing and clunky. We need to make it easy for our self-hosted and gitlab.com users to renew. 
* We aren't utilizing industry standard dunning processes for credit card updates and retries. We should work to automatically update expired credit cards and provide better messaging to customers so they aren't suprised when a renewal fails. 
* We ship a ton of features and enhancements each month. We need to do a better job at driving awareness and adoption of these features so that our useres can gain the full value of our offering. 
* Activation is the foundation for retention. We need to help customers early in their lifecycle with GitLab discover and experience the full value of GitLab as a complete DevOps platform. 
* We have users that disengage with our product for various reasons. We need to gain a better understanding of those reasons and work in conjunction with Customer Success to help get those users back on track and experiencing the full value of the GitLab offering. 


### Our approach

Retention runs a standard growth process. We gather feedback, analyze the information, ideate, then create experiments and test. As you can imagine the amount of ideas we can come up with is enormous so we use the [ICE framework](https://blog.growthhackers.com/the-practical-advantage-of-the-ice-score-as-a-test-prioritization-framework-cdd5f0808d64) (impact, confidence, effort) to ensure we are focusing on the experiments that will provide the most value. 

Retention's growth model is focused on 4 main areas: 

* Engagement  
* Churn  
* Renewal  
* Resurrection  

[![alt text](/images/growth/growth-model-retention.svg "Retention Growth Model")](/images/growth/growth-model-retention.svg)



### Current Priorities


[![alt text](/images/growth/retention-roadmap-sep-2020.svg "Retention Roadmap")](/images/growth/retention-roadmap-sep-2020.svg)


Previous work: 


<table>
  <tr>
   <td>ICE Score
   </td>
   <td>Focus
   </td>
   <td>Why/Hypothesis
   </td>
   <td> Status
   </td> 
  </tr>
  <tr>
   <td>9.0
   </td>
   <td><a href="https://gitlab.com/gitlab-org/growth/product/issues/43">Disable ability to turn Auto-Renew off for .com and provide a cancel option</a>
   </td>
   <td>.com customers are not aware that disabling auto-renew is essentially the same thing as canceling. If we allow users to cancel (e.g. turn auto-renew off) and explain what they'll lose, we'll see a higher % of subscriptions renew. By collecting feedback during the cancellation process we will also learn of additional opportunities to increase retention. 
   </td>
   <td> <a href="https://gitlab.com/gitlab-org/growth/product/issues/162">AB test a success, pushed to 100%</a>
   </td> 
  </tr>
  <tr>
   <td>8.0
   </td>
   <td><a href="https://gitlab.com/gitlab-org/growth/engineering/issues/42">Charge for `seats currently in use` on GitLab.com when Auto Renew is enabled</a>
   </td>
   <td>When a customer's subscription has the Auto Renew setting toggled to ON, the system will create a new subscription at the renewal date identical to the existing subscription. The problem is, we are losing license money where there are more seats active on GitLab.com than what was in the subscription.
   </td>
   <td> Completed 
   </td>
  </tr>
  <tr>
   <td>7.3
   </td>
   <td><a href="https://gitlab.com/groups/gitlab-org/growth/-/epics/3">Provide better messaging and context in the renewal banners</a>
   </td>
   <td>The current renewal  banners have a number of problems. This work will address the ability to dismiss the banner, linking customers to the portal, handling auto-renew with different messaging, and improving the look and feel of the banner.
   </td>
   <td>Mostly complete, some remaining enhnacements 
   </td>
  </tr>
  <tr>
   <td> 7.0
   </td>
   <td><a href="https://gitlab.com/gitlab-org/growth/product/-/issues/1486">Add and Update Renewal Notifications</a>
   </td>
   <td>We currently only send one renewal notification to customers 30 days before their renewal. Some customers require more advanced notice. This change adds multiple notifications over a 90 day window with customized instructions that are specific to the customer and their subscription.  
   </td> 
   <td> Completed 
   </td>
  </tr>

  <tr>
  <td> 6.5
   </td>
   <td><a href="https://gitlab.com/gitlab-org/growth/product/issues/279">What's New Notification</a>
   </td>
   <td>We believe that by surfacing a notification icon/interaction within gitlab which highlights our recent product enhancements, we'll be able to drive additional awareness and engagement with new features, leading to more engaged accounts and increased retention.
   </td> 
   <td> <a href="https://gitlab.com/gitlab-org/gitlab/-/issues/255349">MVC released on GitLab.com.</a> Second iteration in design.  
   </td>
  </tr>
</table>




### Helpful Links

[GitLab Growth project](https://gitlab.com/gitlab-org/growth)  
[Retention Kanban Board](https://gitlab.com/groups/gitlab-org/-/boards/1642224?label_name[]=group%3A%3Aretention)  
[KPIs & Performance Indicators](https://about.gitlab.com/handbook/product/metrics/)  

